% Display the Xsecs as exported via MultiBolt's exporter for versions 3.0.1
% and above.

function [f] = print_exported_Xsecs(export_path)

    f = nan; 
    
    
    D = [export_path, '/Xsecs'];
    
    if ~isfolder(D)
        disp(['Cannot display cross-sections in ', export_path, ': folder /Xsecs does not exist.']);
        return
    end
    
    leg_tags = {};
    handles = {};
    
    f = figure;
    hold on;
    xlim([1e-3, 1e+3])
    set(gca, 'xscale', 'log', 'yscale', 'log')
    xlabel('Electron Energy [eV]')
    ylabel('\sigma [m^2]')
    
    S = dir(fullfile(D,'*'));
    N = setdiff({S([S.isdir]).name},{'.','..'}); % list of subfolders of D.
    for ii = 1:numel(N)
        T = dir(fullfile(D,N{ii},'*')); % improve by specifying the file extension.
        C = {T(~[T.isdir]).name}; % files in subfolder.
        for jj = 1:numel(C)
            F = fullfile(D,N{ii},C{jj});
          
            
            fid = fopen ( F );
            if fid ~= -1
              for i=1:3
                fgetl ( fid );
              end
              leg_tags{end+1} = fgetl ( fid );
              leg_tags{end} = replace(leg_tags{end}, '# Process: ', '');
              fclose ( fid );
            end
            
            
            mat = readmatrix(F);
            handles{end+1} = plot(mat(:,1), mat(:,2));
            
            
            
        end
    end
    
    leg = legend(leg_tags);

end