// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

// TODO: whole file probably belongs to Exporter project

// if you want MultiBolt to export more things per-run (eedf level), push_back an object here
void mb::Exporter::create_perRun_exportables() {

	std::string f0_LEVEL = full_path.string() + mb::sep() + "EEDFs_f0" + mb::sep();
	std::string f1_LEVEL = full_path.string() + mb::sep() + "EEDFs_f1" + mb::sep();

	perRun_exportables = {}; // reset



	std::string f_col1_val = mb::mb_format(col1_val);


	if (this->export_limited == true) {
		// in limited: nothing prints here.
		return;
	}
	



	perRun_exportables.push_back(
		exportable_f{
			"f0_" + std::to_string(sweep_idx),
			{{"The ell = 0 distribution, the isotropy."} ,
				{ col1_var + " " + col1_units + "\t" + f_col1_val}, // <- needs format
				{"col_1: Electron Energy [eV], col_2: f0 [eV^(-3/2)]" } },
			f0_LEVEL,
			"eV",
			"f0",
			out.eV_even,
			out.f0 }
	);

	perRun_exportables.push_back(
		exportable_f{
			"f1_" + std::to_string(sweep_idx),
			{{"The ell = 1 distribution, the first anisotropy."},
				{ col1_var + " " + col1_units + "\t" + f_col1_val},
				{"col_1: Electron Energy [eV], col_2: f1 [eV^(-3/2)]" } },
			f1_LEVEL,
			"eV",
			"f1",
			out.eV_odd,
			out.f1 }
	);



	return;
}
