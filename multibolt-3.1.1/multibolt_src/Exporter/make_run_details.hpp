// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// develop run_details.txt
void mb::Exporter::make_run_details() {

	

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	std::ofstream fout;

	fout.open(fs::path(this->full_path.string() + mb::sep() + "RUN_DETAILS.txt"), std::ofstream::out);

	fout << "Export location: " << this->full_path << "\n";
	fout << "Exported to: " << this->export_name << "\n";
	fout << "Date: " << mb::print_datetime() << "\n";

	fout << "\n";

	fout << "Model: " << mb::model_map.find(p.model)->second << "\n";


	if (sweep_op == mb::sweep_option::Nu) {
		fout << "Energy Points: < SWEEP >\n";
	}
	else {
		fout << "Energy Points: " << std::to_string(p.Nu) << "\n";

	}


	if (sweep_op == mb::sweep_option::N_terms) {
		fout << "Number of terms: < SWEEP >\n";
	}
	else {
		fout << "Number of terms: " << std::to_string(p.N_terms) << "\n";
	}

	
	fout << "USE_ENERGY_REMAP: " << std::boolalpha << p.USE_ENERGY_REMAP << "\n";
	fout << "Desired EEDF log-height: " << std::to_string(p.remap_target_order_span) << "\n";
	fout << "Max # of remap grid-trials: " << std::to_string(p.remap_grid_trial_max) << "\n";




	if (sweep_op == mb::sweep_option::EN_Td) {
		fout << "E/N [Td]: < SWEEP >\n";
	}
	else {
		fout << "E/N [Td]: " << mb::mb_format(p.EN_Td) << "\n";
	}

	if (sweep_op == mb::sweep_option::T_K) {
		fout << "Temperature [K]: < SWEEP >\n";
	}
	else {
		fout << "Temperature [K]: " << mb::mb_format(p.T_K) << "\n";
	}


	
		if (p.T_K == 0) {
			fout << "Particle Density [m**-3]: " << "<COLD CASE>" << mb::mb_format(101325.0 / mb::KB / (300.0) * (p.p_Torr / 760.0)) << "\n";
		}
		else {
			fout << "Particle Density [m**-3]: " << mb::mb_format(101325.0 / mb::KB / (p.T_K) * (p.p_Torr / 760.0)) << "\n";
		}
	
	fout << "USE_EV_MAX_GESS: " << std::boolalpha << p.USE_EV_MAX_GUESS << "\n";
	fout << "Preset eV Max: " << mb::mb_format(p.initial_eV_max) << "\n";

	fout << "Convergence Error: " << mb::mb_format(p.conv_err) << "\n";
	fout << "Max iterations: " << std::to_string(p.iter_max) << "\n";
	fout << "Min iterations: " << std::to_string(p.iter_min) << "\n";

	fout << "fNEW: " << mb::mb_format(p.weight_f0) << "\n";

	fout << "Ionization energy sharing: " << mb::mb_format(p.sharing) << "\n";

	fout << "Shy mode: " << std::boolalpha << p.SHY << "\n";

	fout << "Allow sum(fracs)!=1: " << std::boolalpha << p.DONT_ENFORCE_SUM << "\n";

	fout << "\n";

	for (auto it = lib.allspecies.begin(); it != lib.allspecies.end(); ++it) {

		auto Ng = it - lib.allspecies.begin();

		fout << "\tSpecies # " << Ng << "\n";
		fout << "\t\tName: " << (*it)->name() << "\n";

		if (this->sweep_op == mb::sweep_option::bin_frac && Ng == 0) {
			fout << "\t\tFraction [1/1]: < SWEEP >\n";
		}
		else {
			fout << "\t\tFraction [1/1]: " << (*it)->frac() << "\n";
		}

		fout << "\t\tAttachment Collision Cross Sections\n";
		for (auto it2 = (*it)->att.begin(); it2 != (*it)->att.end(); it2++) {
			auto i = it2 - (*it)->att.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() <<  "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}

		fout << "\t\tElastic Collision Cross Sections\n";
		for (auto it2 = (*it)->ela.begin(); it2 != (*it)->ela.end(); it2++) {
			auto i = it2 - (*it)->ela.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() << "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}

		fout << "\t\tEffective Collision Cross Sections\n";
		for (auto it2 = (*it)->eff.begin(); it2 != (*it)->eff.end(); it2++) {
			auto i = it2 - (*it)->eff.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() << "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}

		fout << "\t\tExcitation Collision Cross Sections\n";
		for (auto it2 = (*it)->exc.begin(); it2 != (*it)->exc.end(); it2++) {
			auto i = it2 - (*it)->exc.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() << "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}

		fout << "\t\tIonization Collision Cross Sections\n";
		for (auto it2 = (*it)->iz.begin(); it2 != (*it)->iz.end(); it2++) {
			auto i = it2 - (*it)->iz.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() << "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}

		fout << "\t\tSuper-elastic Collision Cross Sections\n";
		for (auto it2 = (*it)->sup.begin(); it2 != (*it)->sup.end(); it2++) {
			auto i = it2 - (*it)->sup.begin();
			fout << "\t\t\t" << std::to_string(i) << "\tx" << (*it2)->scale() << "\t" << (*it2)->process() << "\t" << (*it2)->scattering()->name << "\n";
		}


	}

	mb::debug_statement("Created " + this->full_path.string() + mb::sep() + "RUN_DETAILS.txt");

	return;
}

// make species_idx.txt
void mb::Exporter::make_species_indices() {

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;

	std::ofstream fout;

	if (this->export_limited == true) {
		return; // don't want to print in limited case
	}


	fout.open(fs::path(this->full_path.string() + mb::sep() + "species_indices.txt"), std::ofstream::out);

	fout << "# The indices for particular species\n";
	fout << "# col_1: species_idx [count], col_2: species_name\n";
	fout << "species_idx\tspecies_name\n";

	for (auto it = lib.allspecies.begin(); it != lib.allspecies.end(); it++) {
		auto i = it - lib.allspecies.begin();
		fout << std::to_string(i) << "\t" << (*it)->name() << "\n";
	}


	fout.close();


	mb::debug_statement("Created " + this->full_path.string() + mb::sep() + "species_indices.txt");


};

void mb::Exporter::make_collision_indices() {

// todo?

	return;
}
