// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

// to do: Exporter cleanup

// Discern how to talk about the independent variables
// based on passed code
void mb::Exporter::pick_based_on_sweep() {
	switch (this->sweep_op) {

	case mb::sweep_option::none:
		this->col1_var = "E_N";
		this->col1_units = "[Td]";
		this->col1_val = p.EN_Td;
		break;

	case mb::sweep_option::EN_Td:
		this->col1_var = "E_N";
		this->col1_units = "[Td]";
		this->col1_val = p.EN_Td;
		break;

	case mb::sweep_option::bin_frac: // the driving species is always the first
		this->col1_var = "fraction_" + lib.allspecies[0]->name();
		this->col1_units = "[1/1]";
		this->col1_val = lib.allspecies[0]->frac();
		break;


	case mb::sweep_option::T_K:
		this->col1_var = "T";
		this->col1_units = "[K]";
		this->col1_val = p.T_K;
		break;

	case mb::sweep_option::Nu:
		this->col1_var = "Nu";
		this->col1_units = "[#]";
		this->col1_val = p.Nu;
		break;

	case mb::sweep_option::N_terms:
		this->col1_var = "N_terms";
		this->col1_units = "[#]";
		this->col1_val = p.N_terms;
		break;

	}




	return;
}