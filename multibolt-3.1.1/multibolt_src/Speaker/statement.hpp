// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"


inline bool error_statement(const std::string msg) {
#ifdef MULTIBOLT_USING_OPENMP


	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) { // print as long as we are not SILENT
#pragma omp critical
		std::cout << "\nmb: thrID: *** ERROR *** " + std::to_string(omp_get_thread_num()) + "\t| " << msg;
		fflush(stdout);
	}

#else

	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) { // print as long as we are not SILENT
		std::cout << "\nmb: thrID: *** ERROR *** " + std::to_string(0) + "\t| " << msg;
		fflush(stdout);
	}

#endif

	return true;
}

inline bool normal_statement(const std::string msg) {

	
#ifdef MULTIBOLT_USING_OPENMP


	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) { // print as long as we are not SILENT
		#pragma omp critical
		std::cout << "\nmb: thrID: " + std::to_string(omp_get_thread_num()) + "\t| " << msg;
		fflush(stdout);
	}

#else

	if (mb::mbSpeaker.get_outOp() != mb::OutputOption::SILENT) { // print as long as we are not SILENT
		std::cout << "\nmb: thrID: " + std::to_string(0) + "\t| " << msg;
		fflush(stdout);
	}

#endif

	return true;
}

inline bool debug_statement(const std::string msg) {

#ifdef MULTIBOLT_USING_OPENMP

	if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) { // print as long as we are not SILENT
#pragma omp critical
		std::cout << "\nmb: thrID: debug: " + std::to_string(omp_get_thread_num()) + "\t| " << msg;
		fflush(stdout);
	}

#else

if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) { // print as long as we are not SILENT
	std::cout << "\nmb: thrID: debug: " + std::to_string(0) + "\t| " << msg;
	fflush(stdout);
}

#endif

	return true;
}


