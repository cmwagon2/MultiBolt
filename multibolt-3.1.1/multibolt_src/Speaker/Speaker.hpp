// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// Yes, this is a non-constant global variable
// I believe at this time that the complexity reduced by this being global is worth the cost
// it is only an output option controller: it cannot itself break anything
class Speaker {

private:
	mb::OutputOption outOp = mb::OutputOption::NORMAL;

public:
	void printmode_normal_statements() { outOp = mb::OutputOption::NORMAL; return; }
	void printmode_debug_statements() { outOp = mb::OutputOption::DEBUG; return; }
	void printmode_no_statements() { outOp = mb::OutputOption::SILENT; return; }

	mb::OutputOption get_outOp() { return outOp; }
};

Speaker mbSpeaker = Speaker(); // Initially using debug mode. To change, call global.printmode_normal_statements(), etc.

