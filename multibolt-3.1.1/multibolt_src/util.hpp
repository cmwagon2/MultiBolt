// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


#pragma once
#include "multibolt"



// Contains useful tools for multibolt-side functions

inline double err(const double& now, const double& prev) {
	double err = abs(1 - abs(now / prev));

	if (arma::is_finite(err)) {
		return err;
	}
	else {
		return arma::datum::nan;
	}
	
}

// find the mean squared error
inline double f_MSE(const arma::colvec& f_a, const arma::colvec& f_b) {


	auto N = f_a.n_elem;

	if (f_a.n_elem != f_b.n_elem) {
		return arma::datum::nan;
	}

	return 1.0 / N * arma::sum(pow(f_a - f_b, 2));
}


// placeholder expression for whether the eigenvalue has converged to satisfaction or not
inline bool converged(const double& conv_err, const double& now, const double& prev) {
	if (mb::err(now, prev) < conv_err) {
		return true;
	}
	return false;
}

inline bool check_did_not_converge(const std::string& scheme, const int& iter, const int& iter_max) {

	char buff[300];

	if (iter >= iter_max) {
		snprintf(buff, sizeof(buff), "Scheme %s did not converge in %d iterations.", scheme.c_str(), iter_max);
		mb::normal_statement(buff);
	}
	else {
		snprintf(buff, sizeof(buff), "Scheme %s converged in %d iterations.", scheme.c_str(), iter);
		mb::normal_statement(buff);
	}

	return true;

}
	

bool display_iteration_banner(const std::string& scheme, const int& iter, const std::string& name, const double& value, const double& err ) {

	char buff[300];
	snprintf(buff, sizeof(buff), "%s\t| i: %d | %s: %1.5e | err: %1.5e", scheme.c_str(), iter, name.c_str(), value, err);
	mb::normal_statement(buff);
	return true;
}


// assist: return the seperator character perferred by your system
inline std::string sep() {
	namespace fs = std::filesystem;
	return fs::path("/").make_preferred().string();
}

// return standard delimeter I've chosen
inline std::string delim() {
	return "\t";
}


// TODO: move to exporter
// prepare a file by making its header and then leaving
inline bool init_txt(const std::string& path, const std::string& filename, const std::vector<std::string>& headerlines, const std::string& key1, const std::string& key2) {

	namespace fs = std::filesystem;

	std::string extension = "txt";
	std::string full_filename = filename + "." + extension;
	fs::path full_path = fs::path(path).make_preferred().lexically_normal();


	if (!fs::is_directory(full_path)) {
		mb::error_statement("Did not init file '" + full_filename + "' because the directory '" + full_path.string() + "' does not exist.");
		return false;
	}

	if (fs::exists(full_filename)) {
		mb::error_statement("File '" + full_filename + "' already exists in the directory'" + full_path.string() + "' and will not be overwritten.");
		return false;
	}


	std::ofstream fout;


	fout.open(full_path.string() + mb::sep() + full_filename, std::ofstream::out);

	// write header lines preceded by comments
	for (auto& line : headerlines) {
		fout << "# " << line << "\n";
	}

	// write the un-commented line which represents the variable names
	fout << key1 << mb::delim() << key2 << "\n";

	fout.close();

	return true;
}



// TODO: move to exporter
// given that the data file already exists and already has its header, allow putting some stuff on the bottom
inline bool append_to_txt(const std::string& path, const std::string& filename, const arma::colvec& leftcol, const arma::colvec& rightcol) {

	// if mat is of unexpected size, throw a fit
	//if (data.n_cols != 2) {
	//	mb::error_statement("Cannot append data because it is not in a two-column matrix form");
	//	return false;
	//}


	namespace fs = std::filesystem;
	std::string extension = "txt";

	fs::path full_file_path = fs::path(path + mb::sep() + filename + "." + extension).make_preferred().lexically_normal();



	// if file already exists at the path
	if (!fs::exists(full_file_path)) {
		mb::error_statement("The file '" + full_file_path.string() + "' doesn't exist and can't be appended.");
		return false;
	}

	std::fstream fout;

	fout.open(full_file_path, std::fstream::app);

	char buff[100];

	for (int n = 0; n < leftcol.n_rows; ++n) {

		snprintf(buff, sizeof(buff), mb::FORMAT_CODE.c_str(), leftcol(n));
		std::string left = buff;

		snprintf(buff, sizeof(buff), mb::FORMAT_CODE.c_str(), rightcol(n));
		std::string right = buff;

		fout << left << mb::delim() << right << "\n";
	}

	fout.close();

	return true;
}


std::string print_datetime() {
	std::time_t now = time(0);
	std::tm* ltm = localtime(&now);

	int year = ltm->tm_year + 1900;
	int month = ltm->tm_mon + 1;
	int day = ltm->tm_mday;

	int hour = ltm->tm_hour;

	if (hour > 12) {
		hour = hour - 12;
	}

	char halve;
	if (ltm->tm_hour <= 11) {
		halve = 'A';
	}
	else {
		halve = 'P';
	}

	int min = ltm->tm_min;
	int sec = ltm->tm_sec;

	char buffer[100];

	snprintf(buffer, sizeof(buffer), "%d/%d/%d %d:%d:%d %cM", month, day, year, hour, min, sec, halve);

	return std::string(buffer);
}


bool is_even_or_zero(int val) {
	return (val % 2 == 0 || val == 0);
}
bool is_even_or_zero(arma::uword val) {
	return (val % 2 == 0 || val == 0);
}
bool is_even_or_zero(arma::sword val) {
	return (val % 2 == 0 || val == 0);
}

bool doubles_are_same(double x, double y) {

	if (x == 0 && y == 0) {
		return true; // if both are definitely zero
	}

	if (abs(x - y)/abs(x) < 1e-30) { // is the difference between the doubles essentially zero?
		return true;
	}

		return false;

}

bool is_negative(double x) {
	return (x > 0) ? true : false;
}

bool cols_are_same_length(arma::colvec A, arma::colvec B) {
	return (A.n_elem == B.n_elem) ? true : false;
}



// helper function, cut down on some clutter
bool same_string(std::string A, std::string B) {

	if (A.compare(B) == 0) {
		return true;
	}
	else {
		return false;
	}
}




// trim from start (in place)
inline void ltrim(std::string& s) {
	s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !std::isspace(ch);
		}));
}

// trim from end (in place)
inline void rtrim(std::string& s) {
	s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
		return !std::isspace(ch);
		}).base(), s.end());
}

// trim from both ends (in place)
inline void trim(std::string& s) {
	mb::ltrim(s);
	mb::rtrim(s);
}

// trim from start (copying)
inline std::string ltrim_copy(std::string s) {
	mb::ltrim(s);
	return s;
}

// trim from end (copying)
inline std::string rtrim_copy(std::string s) {
	mb::rtrim(s);
	return s;
}

// trim from both ends (copying)
inline std::string trim_copy(std::string s) {
	mb::trim(s);
	return s;
}


std::vector<std::string> split(std::string str, std::string token) {
	std::vector<std::string>result;
	while (str.size()) {
		auto index = str.find(token);
		if (index != std::string::npos) {
			result.push_back(str.substr(0, index));
			str = str.substr(index + token.size());
			if (str.size() == 0)result.push_back(str);
		}
		else {
			result.push_back(str);
			str = "";
		}
	}
	return result;
}

// space-saver
bool is_collision_string(const std::string& line) {
	if (mb::same_string(line, lib::ATTACHMENT_STRING) ||
		mb::same_string(line, lib::ELASTIC_STRING) ||
		mb::same_string(line, lib::EFFECTIVE_STRING) ||
		mb::same_string(line, lib::EXCITATION_STRING) ||
		mb::same_string(line, lib::IONIZATION_STRING)) {

		return true;
	}

	return false;
}




bool isInt(const std::string& s)
{
	// allow first character to be + or -
	char first = s.c_str()[0];
	if (!(first == '+' || first == '-' || std::isdigit(first))) {
		return false;
	}

	for (char const& ch : s) {
		if (std::isdigit(ch) == 0)
			return false;
	}

	return true;
}


bool isFloat(const std::string& s)
{

	// allow first character to be + or - or .
	char first = s.c_str()[0];
	if (!(first == '+' || first == '-' || std::isdigit(first) || first == '.')) {
		return false;
	}

	// case for scientific notation: does no more than 1 e or 1 . exist?
	if (std::count(s.begin(), s.end(), 'e') > 1 ||
		std::count(s.begin(), s.end(), '.') > 1) {
		return false;
	}

	// and beyond the first character, no more than one + or - is present
	if (std::count(s.begin()+1, s.end(), '+') > 1 ||
		std::count(s.begin()+1, s.end(), '-') > 1) {
		return false;
	}


	for (auto it = s.begin(); it != s.end(); it++) {
		if (!(std::isdigit(*it) || *it == 'e' || *it == '.' || *it == '+' || *it == '-')) {
			return false;
		}
	}


	return true;
}



std::string mb_format(const std::string& var) {

	char buff [300];

	snprintf(buff, sizeof(buff), mb::FORMAT_CODE.c_str(), var);

	return buff;

}

std::string mb_format(const double& var){

	char buff[300];

	snprintf(buff, sizeof(buff), mb::FORMAT_CODE.c_str(), var);

	return buff;

}

// prevent users from trying to solve and write to invalid dir specs
bool is_valid_dirstring(std::string dir_string) {

	using std::ifstream;
	using std::stringstream;
	namespace fs = std::filesystem;
	using namespace fs;


	/* ---- */
	// https://stackoverflow.com/questions/10159230/validating-user-input-for-filenames
	char bad_chars[] = "!@%^*~|";
	char invalid_found = false;
	int i;
	for (i = 0; i < strlen(bad_chars); ++i) {
		if (strchr(dir_string.c_str(), bad_chars[i]) != NULL) {
			invalid_found = true;
			break;
		}
	}

	return !invalid_found;

}

// probe for y at x, use adjacent points x1/y1 and x2/y2 
double manual_linterp(double x, double x1, double y1, double x2, double y2) {
	double y = 0;
	y = y1 + (x - x1) * (y2 - y1) / (x2 - x1);
	return y;
}