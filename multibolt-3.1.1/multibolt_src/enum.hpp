// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"

typedef enum class YesNo
{
	Yes = 1,
	No = 0
} YesNo;

typedef enum class ModelCode
{
	SST,
	HD,
	HDGE,
	HDGE_01 // dev: special case: only solve up to W_BULK and ignore D_BULK
} ModelCode;



typedef enum class OutputOption
{
	DEBUG,
	NORMAL,
	SILENT
} OutputOption;

typedef enum class sweep_option {

	EN_Td,
	T_K,
	p_Torr, // deprecated?
	bin_frac,
	Nu,
	N_terms,
	none   // deprecated

} sweep_option;

typedef enum class sweep_style {
	lin,
	log,
	reg,
	def,
	single // deprecated
} sweep_style;

typedef enum class RemapChoice {

	KeepGrid,
	LowerGrid,
	RaiseGridLittle,
	RaiseGridExtrap

} RemapChoice;

// 04/29/2023
typedef enum class GridStyle {
	LinearEnergy, // classic uniform finite-difference on energy grid
	QuadraticEnergy // ie, uniform velocity // for testing only, do not use in-general!
} GridStyle;











	