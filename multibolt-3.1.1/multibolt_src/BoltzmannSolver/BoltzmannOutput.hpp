// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// A BoltzmannOutput is the struct-like container for everything one cares about
// when it comes to  MultiBolt calculation results.
//
// theres an internal object holding the "totals" (the rate/growth coefficients), and one internal object per-species holding the per-species data
// Distributions are more general and contained in toplevel

#pragma once

#include "multibolt"


// One exists per-gas
class BoltzmannOutputPerGas{

public:

	std::string name;

	// Both HD and SST
	arma::vec k_ela_N = { };	// Elastic rate [m^3 s]. Indices align with order of s_ela processes
	arma::vec k_eff_N = { };	// Elastic rate [m^3 s]. Indices align with order of s_eff processes
	arma::vec k_exc_N = { };	// Excitation rate [m^3 s]. Indices align with order of s_exc processes
	arma::vec k_iz_N = { };		// Ionization rate [m^3 s]. Indices align with order of s_iz processes
	arma::vec k_att_N = { };	// Attachment rate [m^3 s]. Indices align with order of s_att process
	arma::vec k_sup_N = { };	// Super-elastic rate [m^3 s]. Indices align with order of s_sup process


	// SST-only
	arma::vec alpha_N = { };	// Ionization coefficient [m^2]. Indices align with order of s_iz processes
	arma::vec eta_N = { };		// Attachment coefficient [m^2]. Indices align with order of s_att processes

};




// 
class BoltzmannOutputTotal {

public:
	// Both HD and SST
	double k_ela_N = arma::datum::nan;					// Total normalized elastic rate [m^3 s]
	double k_eff_N = arma::datum::nan;					// Total normalized effective rate [m^3 s]
	double k_exc_N = arma::datum::nan;					// Total normalized excitation rate [m^3 s]
	double k_iz_N = arma::datum::nan;					// Total normalized ionization rate [m^3 s]
	double k_att_N = arma::datum::nan;					// Total normalized attachment rate [m^3 s]
	double k_sup_N = arma::datum::nan;					// Total normalized attachment rate [m^3 s]

	// SST conditions only
	double alpha_N = arma::datum::nan;					// Total primary ionization coefficient [m^2]
	double eta_N = arma::datum::nan;					// Total attachment coefficient [m^2]

};



// There will be one MultiBoltOutput object per BE solution. The export-handler will work with these to write the MultiBoltOutput.
class BoltzmannOutput {

public:

	double avg_en = arma::datum::nan;					// Mean electron energy [eV]
	double D_mu = arma::datum::nan;						// Characterisitc electron energy (f0 only)
	double DT_mu = arma::datum::nan;					// Characterisitc electron energy (T only)
	double DL_mu = arma::datum::nan;					// Characterisitc electron energy (L only)


	double muN_SST = arma::datum::nan;
	double muN_FLUX = arma::datum::nan;					// Flux portion of the particle mobility
	double muN_BULK = arma::datum::nan;
	
	double energy_muN = arma::datum::nan;				// same as above but for energy

	double DN_f0 = arma::datum::nan;					// Conventional expression for particle diffusion coefficient (for electrons). Does not account for non-conservative effects.
	double DN_f0_nu = arma::datum::nan;					// Same as above, includes non-conservative
	
	double energy_DN_f0 = arma::datum::nan;				// same as above but for energy
	double energy_DN_f0_nu = arma::datum::nan;

	double k_iz_eff_N = arma::datum::nan;				// Normalized effective ionization rate [m^3 s]

	double alpha_eff_N = arma::datum::nan;				// Normalized total effective primary ionization (Townsend) coefficient [m^2] 
														// In SST conditions : exact, equivalent to W_SST/k_iz_eff_N
														// In HD conditions: Found using Blevin_Fletcher relation applying W, k_iz_eff_N, and DL

	double W_SST = arma::datum::nan;					// Drift velocity particular for steady state Townsend condition [m/s]

	// HD+Gradient-expansion dependent
	double W_FLUX = arma::datum::nan;					// Flux portion of the drift velocity. [m/s]
	double W_BULK = arma::datum::nan;					// Bulk drift velocity [m/s]

	double DTN_FLUX = arma::datum::nan;					// Flux portion of normalized transverse diffusion coefficient [1/m^2 /s]
	double DLN_FLUX = arma::datum::nan;					// Flux portion of normalized longitudinal diffusion coefficient [1/m^2 /s]

	double DLN_BULK = arma::datum::nan;				// Normalized bulk longitudinal diffusion coefficient [1/m^2 /s]
	double DTN_BULK = arma::datum::nan;				// Normalized bulk transverse diffusion coefficient [1/m^2 /s]




	// At least to start, only ever share f0 anbd f1 of the orders

	// Energy grid
	arma::colvec eV_even = {};
	arma::colvec eV_odd = {};

	// Distribution Functions
	arma::colvec f0 = { };		// Isotropy. col-0 : eV, col-1: EEDF
	arma::colvec f1 = { };		// First anisotropy. Useful for comparing to other simulations.  col-0 : eV, col-1: EEDF

	// todo: consider also outputting dist. functions of higher orders?

	mb::BoltzmannParameters p;


	double eV_max = arma::datum::nan;

	std::vector<mb::BoltzmannOutputPerGas> per_gas;	// Per-gas processes accessed via this member struct (indices: Ng)

	mb::BoltzmannOutputTotal total;						// Total processes accessed via this member struct
	//mb::output_shared shared;

	double time = arma::datum::nan; // in s

	// todo: work with this
	//std::vector<std::string> errors; // Strings display written errors. Vector is pushed-back with errors as they occur dynamically


	std::string print_str(); // banner-output function
	void print();
	
};



void mb::BoltzmannOutput::print() {

	std::string str = print_str();

	mb::normal_statement("\n"+str);

	return;
}



// Print materials that this output object currently holds
// This is more useful as a debugging tool rather than a real output.
std::string mb::BoltzmannOutput::print_str() {

	char buff[300];
	std::string str = "";


	//if (do_vals_total) {
		
		//-----
		str = str + "\tMISC.\n";

		snprintf(buff, sizeof(buff), "\tCalc. Time [s]:\t\t\t%.5e\n", time);
		str = str + std::string(buff);

		str = str + "\tCONDITIONS\n";
		str = str + std::string(p.print_str());


		str = str + "\n\tSWARM ENERGY\n";
		snprintf(buff, sizeof(buff), "\tavg_en [eV]:\t\t\t%.5e\n", avg_en);
		str = str + std::string(buff);
		snprintf(buff, sizeof(buff), "\tD_mu [eV]:\t\t\t%.5e\n", D_mu);
		str = str + std::string(buff);
		snprintf(buff, sizeof(buff), "\tDT_mu [eV]:\t\t\t%.5e\n", DT_mu);
		str = str + std::string(buff);
		snprintf(buff, sizeof(buff), "\tDL_mu [eV]:\t\t\t%.5e\n", DL_mu);
		str = str + std::string(buff);

		//---
		str = str + "\n\tTOTAL RATE COEFFICIENTS\n";
		snprintf(buff, sizeof(buff), "\tk_ela_N [m**3/s]:\t\t%.5e\n", total.k_ela_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_eff_N [m**3/s]:\t\t%.5e\n", total.k_eff_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_exc_N [m**3/s]:\t\t%.5e\n", total.k_exc_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_iz_N [m**3/s]:\t\t%.5e\n", total.k_iz_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_att_N [m**3/s]:\t\t%.5e\n", total.k_att_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_sup_N [m**3/s]:\t\t%.5e\n", total.k_sup_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tk_iz_eff_N [m**3/s]:\t\t%.5e\n", k_iz_eff_N);
		str = str + std::string(buff);

		//----
		str = str + "\n\tTOTAL GROWTH COEFFICIENTS\n";

		snprintf(buff, sizeof(buff), "\talpha_N [m**2]:\t\t\t%.5e\n", total.alpha_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\teta_N [m**2]:\t\t\t%.5e\n", total.eta_N);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\talpha_eff_N [m**2]:\t\t%.5e\n", alpha_eff_N);
		str = str + std::string(buff);

		//-------
		str = str + "\n\tELECTRON MOBILITY\n";

		snprintf(buff, sizeof(buff), "\tmuN_SST [1/m/s/V]:\t\t%.5e\n", muN_SST);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tmuN_FLUX [1/m/s/V]:\t\t%.5e\n", muN_FLUX);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tmuN_BULK [1/m/s/V]:\t\t%.5e\n", muN_BULK);
		str = str + std::string(buff);

		//-------
		str = str + "\n\tENERGY MOBILITY\n";

		snprintf(buff, sizeof(buff), "\tenergy_muN [1/m/s/V]:\t\t%.5e\n", energy_muN);
		str = str + std::string(buff);



		//------
		str = str + "\n\tDRIFT VELOCITY\n";

		snprintf(buff, sizeof(buff), "\tW_SST [m/s]:\t\t\t%.5e\n", W_SST);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tW_FLUX [m/s]:\t\t\t%.5e\n", W_FLUX);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tW_BULK [m/s]:\t\t\t%.5e\n", W_BULK);
		str = str + std::string(buff);



		//-------
		str = str + "\n\tELECTRON DIFFUSION COEFFICIENTS\n";

		snprintf(buff, sizeof(buff), "\tDN [1/m/s]:\t\t\t%.5e\n", DN_f0);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tDN_nu [1/m/s]:\t\t\t%.5e\n", DN_f0_nu);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tDTN_FLUX [1/m/s]:\t\t%.5e\n", DTN_FLUX);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tDTN_BULK [1/m/s]:\t\t%.5e\n", DTN_BULK);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tDLN_FLUX [1/m/s]:\t\t%.5e\n", DLN_FLUX);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tDLN_BULK [1/m/s]:\t\t%.5e\n", DLN_BULK);
		str = str + std::string(buff);


		//-------
		str = str + "\n\tENERGY DIFFUSION COEFFICIENTS\n";

		snprintf(buff, sizeof(buff), "\tenergy_DN [1/m/s]:\t\t%.5e\n", energy_DN_f0);
		str = str + std::string(buff);

		snprintf(buff, sizeof(buff), "\tenergy_DN_nu [1/m/s]:\t\t%.5e\n", energy_DN_f0_nu);
		str = str + std::string(buff);


	//}


	//if (do_per_gas) {

		str = str + "\n\tSPECIFIC RATE COEFFICIENTS\n";
		

		//std::string tag_top = "\t\t\tC#\tk_N [m**3/s]\n";
		std::string tag_bottom = "\t\t\tC%d\t%.5e\n";

		// gas, below, is for an output.per_gas object
		for (int Ng = 0; Ng < per_gas.size(); ++Ng) {

			auto& gas = per_gas.at(Ng);

			snprintf(buff, sizeof(buff), "\tSpecies #%d : %s\n", Ng, gas.name.c_str());
			str = str + std::string(buff);

			str = str + "\t\tELASTIC\n";
			str = str + "\t\t\tC#\tk_ela_N [m**3/s]\n";
			for (int i = 0; i < gas.k_ela_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_ela_N(i));
				str = str + std::string(buff);
			}

			str = str + "\t\tEFFECTIVE\n";
			str = str + "\t\t\tC#\tk_eff_N [m**3/s]\n";
			for (int i = 0; i < gas.k_eff_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_eff_N(i));
				str = str + std::string(buff);
			}

			str = str + "\t\tEXCITATION\n";
			str = str + "\t\t\tC#\tk_exc_N [m**3/s]\n";
			for (int i = 0; i < gas.k_exc_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_exc_N(i));
				str = str + std::string(buff);
			}

			str = str + "\t\tIONIZATION\n";
			str = str + "\t\t\tC#\tk_iz_N [m**3/s]\n";
			for (int i = 0; i < gas.k_iz_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_iz_N(i));
				str = str + std::string(buff);
			}

			str = str + "\t\tATTACHMENT\n";
			str = str + "\t\t\tC#\tk_att_N [m**3/s]\n";
			for (int i = 0; i < gas.k_att_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_att_N(i));
				str = str + std::string(buff);
			}

			str = str + "\t\tSUPERELASTIC\n";
			str = str + "\t\t\tC#\tk_sup_N [m**3/s]\n";
			for (int i = 0; i < gas.k_sup_N.n_elem; ++i) {
				snprintf(buff, sizeof(buff), tag_bottom.c_str(), i, gas.k_sup_N(i));
				str = str + std::string(buff);
			}

			str = str + std::string("\t\t-------------------------\n");

		}

		str = str + "\n\tDISTRIBUTION FUNCTIONS\n";
		str = str + "\ti\teV\t\tf0\t\teV\t\tf1\n";

		auto N = f0.n_rows;
		for (int i = 0; i < N; ++i) {

			
			snprintf(buff, sizeof(buff), "\t%d\t%.5e\t%.5e\t%.5e\t%.5e\n", i, eV_even(i), f0(i), eV_odd(i), f1(i));
			str = str + std::string(buff);

			//mb::normal_statement(buff);
		}

	return str;
}





