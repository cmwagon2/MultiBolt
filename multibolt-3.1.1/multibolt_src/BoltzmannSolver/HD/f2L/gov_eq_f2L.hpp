// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


void mb::BoltzmannSolver::gov_eq_f2L(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b) {

	// heirarchy convenience
	gov_eq_f0_HD(ell, A);


	double u = 0;

	double f0 = 0;
	double f1L = 0;

	double f1L_plus = 0;
	double f1L_minus = 0;

	double f1T_plus = 0;
	double f1T_minus = 0;

	// todo: figure out how to better abstract this
	for (arma::sword k = 0; k < p.Nu; ++k) {

		arma::sword kcol = k;
		arma::sword kcol2 = k;
		if (ell % 2 == 0 || ell == 0) { // if is even
			u = g.u_e.at(k);
			kcol = k - 1;
			kcol2 = k + 1;
		}
		else { // if is odd instead
			u = g.u_o.at(k);
			kcol = k + 1;
			kcol2 = k - 1;
		}

		f0 = x_f(k + ell * p.Nu);
		f1L = x_f1L(k + ell * p.Nu);

		if (ell < p.N_terms - 1) {
			if (kcol >= 0 && kcol < p.Nu) {
				f1L_plus = (x_f1L(k + (ell + 1) * p.Nu) + x_f1L(kcol + (ell + 1) * p.Nu)) / 2.0;
			}
			else {
				f1L_plus = x_f1L(k + (ell + 1) * p.Nu) / 2.0;
			}
		}
		else {
			f1L_plus = 0;
		}


		if (ell > 0) {
			if (kcol >= 0 && kcol < p.Nu) {
				f1L_minus = (x_f1L(k + (ell - 1) * p.Nu) + x_f1L(kcol + (ell - 1) * p.Nu)) / 2.0;
			}
			else {
				f1L_minus = x_f1L(k + (ell - 1) * p.Nu) / 2.0;
			}
		}
		else {
			f1L_minus = 0;
		}



		if (ell < p.N_terms - 1) {
			if (kcol >= 0 && kcol < p.Nu) {
				f1T_plus = (x_f1T(k + (ell + 1) * p.Nu) + x_f1T(kcol + (ell + 1) * p.Nu)) / 2.0;
			}
			else {
				f1T_plus = x_f1T(k + (ell + 1) * p.Nu) / 2.0;
			}
		}
		else {
			f1T_plus = 0;
		}

		if (ell > 0) {
			if (kcol >= 0 && kcol < p.Nu) {
				f1T_minus = (x_f1T(k + (ell - 1) * p.Nu) + x_f1T(kcol + (ell - 1) * p.Nu)) / 2.0;
			}
			else {
				f1T_minus = x_f1T(k + (ell - 1) * p.Nu) / 2.0;
			}
		}
		else {
			f1T_minus = 0;
		}

		b(k + ell * p.Nu) +=
			-sqrt(u * mb::ME / 2.0) * omega2_bar * f0 * Np
				+ sqrt(u * mb::ME / 2.0) * sqrt(2.0 / 3.0) * omega1 * f1L
					- ell / (2.0 * ell - 1.0) * sqrt(2.0 / 3.0) * u * (f1L_minus + (ell - 1.0) / 2.0 * f1T_minus)
						- (ell + 1.0) / (2.0 * ell + 3.0) * sqrt(2.0 / 3.0) * u * (f1L_plus - (ell + 2.0) / 2.0 * f1T_plus);

	}


}