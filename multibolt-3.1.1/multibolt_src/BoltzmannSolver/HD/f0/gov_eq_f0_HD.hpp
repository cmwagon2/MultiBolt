// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#include "multibolt"


// lambda: governing equation for terms of (ell + 1)
double eq_f0_HD_ell_plus_one(double u, arma::sword ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * (ell + 1.0) / (2.0 * ell + 3.0) * u / Du
		+ mb::QE * E0 * (ell + 1.0) / (2.0 * ell + 3.0) * (ell + 2.0) / 2.0 / 2.0);

};

// lambda: governing equation for terms of (ell - 1)
double eq_f0_HD_ell_minus_one(double u, arma::sword ell, double E0, double Np, double Du, bool is_forward) {
	int SIGN = is_forward ? +1 : -1;
	return (SIGN * mb::QE * E0 * ell / (2.0 * ell - 1.0) * u / Du
		- mb::QE * E0 * ell / (2.0 * ell - 1.0) * (ell - 1.0) / 2.0 / 2.0);

};

// lambda: governing equation for terms of (ell)
double eq_f0_HD_ell(double u, double omega0, double Np) {
	return (Np * omega0 * sqrt(u * mb::ME / (2.0)));
};

void mb::BoltzmannSolver::gov_eq_f0_HD(arma::sword ell, arma::SpMat<double>& A) {

	// in this context, is essentially everything from the original solve_A that excludes the collision operator

	mb::debug_statement("Applying governing equation associated with f0 (HD)");


	arma::colvec u;
	bool forward = true;
	bool backward = false;
	double Du = 0;

	if (ell % 2 == 0 || ell == 0) {
		u = g.u_e;
		
	}
	else {
		u = g.u_o;

		// flip derivative direction
		forward = !(forward);
		backward = !(backward);
	}

	for (arma::sword k = 0; k < p.Nu; ++k) {

		// set index for first derivative effect
		arma::sword kcol = k;

		if (ell % 2 == 0 || ell == 0) { // if is even
			kcol = k - 1;
			Du = Du_at_u(u(k));
		}
		else { // if is odd
			kcol = k + 1;
			Du = Du_at_u(u(k));
		}

		// Apply for f_ell
		A.at(k + (ell * p.Nu), k + (ell * p.Nu)) += eq_f0_HD_ell(u(k), omega0, Np);


		// Apply for ell+1
		if (ell + 1 < p.N_terms) {	// if ell+1 exists
			// f_(l + 1)(k)
			A.at(k + (ell * p.Nu), k + (ell + 1) * p.Nu) += eq_f0_HD_ell_plus_one(u.at(k), ell, E0, Np, Du, forward);

			if (kcol >= 0 && kcol < p.Nu) { // if kcol index exists
				// f_(l + 1)(k - 1)
				A.at(k + (ell * p.Nu), (kcol)+(ell + 1) * p.Nu) += eq_f0_HD_ell_plus_one(u.at(k), ell, E0, Np, Du, backward);
			}
		}

		// Apply for ell - 1
		if (ell - 1 >= 0) {	// if ell-1 exists
			// f_(l - 1)(k)
			A.at(k + (ell * p.Nu), k + (ell - 1) * p.Nu) += eq_f0_HD_ell_minus_one(u.at(k), ell, E0, Np, Du, forward);

			if (kcol >= 0 && kcol < p.Nu) { // if kcol index exists 
				// f_(l - 1)(k + 1)
				A.at(k + (ell * p.Nu), (kcol)+(ell - 1) * p.Nu) += eq_f0_HD_ell_minus_one(u.at(k), ell, E0, Np, Du, backward);
			}
		}
	}


}