// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once

#include "multibolt"

// Attachment contribution to collision operator
// Because attachment tends to be a low-eV process, low-energy electrons will tend to be removed from the system
// This can heat the EEDF (raise dist. height of electrons at higher eV)
// or cool it, for high-threshold attachment (raise dist. height of electrons at lower)


void mb::BoltzmannSolver::C_att(arma::sword ell, arma::SpMat<double>& A) {

	for (auto i_x : g.i_attachment) {

		auto xsec = g.xsec_ptr(i_x);
		auto spec = g.species_ptr(g.i_collision_species(i_x));

		double frac = g.i_frac(i_x);
		if (mb::doubles_are_same(frac, 0)) { continue; }

		mb::debug_statement("Applying C_att[" + std::to_string(ell) + "] with process " + xsec->process() + " in species " + spec->name());

		for (arma::sword k = 0; k < p.Nu; ++k) {
			double u = 0;
			double totalICS = 0;

			if (mb::is_even_or_zero(ell)) {// if ell is even or zero
				u = g.u_e(k);
				totalICS = g.sigma_e(k, i_x);
			}
			else { // if ell is odd
				u = g.u_o(k);
				totalICS = g.sigma_o(k, i_x);
			}

			// electron scatters out and vanishes (non-conservative)
			A.at((ell * p.Nu) + k, (ell * p.Nu) + k) += 
				- frac * Np * totalICS * u;

		}
	}
}

