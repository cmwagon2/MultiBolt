// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// A BoltzmannSolver is the physics owner of most of the math
// Construct one to perform a calculation
// When finished, call .get_output() to receive output object

class BoltzmannSolver {

	public:

		mb::BoltzmannParameters p;
		lib::Library lib; // <- from collisionlibrary
		mb::BoltzmannOutput out;

	private:

		bool solution_succesful = true; // right now, relevant to "SHY"
	
		double Np; // gas environment particle density [m^-3]
		// TODO: if xsec scales become pointers, Np becomes a great way
		// to auto-scale density-dependent xsecs like 3-body attachment in O2

		double E0; // Electric field strength [V/m]

		mb::Grid g; // container for energy-grid objects


		// -- iteration counters
		int iter_f0_SST = 0;
		int iter_f0_HD = 0;
		int iter_f1L = 0;
		int iter_f1T = 0;
		int iter_f2L = 0;
		int iter_f2T = 0;


		// -- Eigenvalues of numerical schemes
		double alpha_eff_N;	// of SST (primary Townsend coefficient)
		double omega0;		// of 0 (effective ionization rate coefficient)
		double omega1;		// of 1L (bulk drift velocity)
		double omega2;		// of 2L (component of longitudinal/transverse diffusion coefficients)
		double omega2_bar;	// of 2T (component of longitudinal/transverse diffusion coefficients)


		// -- Distribution function solutions
		arma::colvec x_f;
		arma::colvec x_f1L;
		arma::colvec x_f1T;
		arma::colvec x_f2L;
		arma::colvec x_f2T;

		// -- Scattering matrix - only need to calculate once per grid.
		// some conditions (many collisions, anisotropic scattering) make the filling of this mat take longer than otherwise
		arma::SpMat<double> A_scattering;

		// -- wall_clock, replacement for MATLABs tic/toc
		arma::wall_clock timer;
		double calculation_time = arma::datum::nan;

		// -- special flags
		bool COLD_CASE = false; // flips if p.T_K is found to be zero, disables thermal contribution
		bool NO_ITERATION = false; // flips if the grid is found to contain no ionization or attachment, ie solutions need only 1 iteration
		bool ZERO_FIELD_CASE = false; // flips if E/N = 0

		
		// -- some condition trackers
		double present_eV_max;
		double present_weight_f0;

	
	public:

		// -- Constructor is the actual execution as long as parameters were actually passed in

		//default, do nothing
		BoltzmannSolver() {};

		
		// -- On construction, perform a MultiBolt calculation
		BoltzmannSolver(const mb::BoltzmannParameters p, const lib::Library lib) {

			timer.tic();

			this->p = p;
			this->lib = lib;
			this->present_eV_max = p.initial_eV_max; // may change during calc due to remap
			this->present_weight_f0 = p.weight_f0; // may change during calc due to stability


			mb::normal_statement("Beginning MultiBolt calculation...");
			mb::debug_statement("With parameters:");
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				this->p.print();
			}

			check_validity();

			if (p.USE_EV_MAX_GUESS == true) {
				this->present_eV_max = this->guess_eV_max();
				mb::normal_statement("Grid Guess: using " + mb::mb_format(present_eV_max) + " eV.");
			}

			if (p.EN_Td == 0) {
				mb::normal_statement("Notice: field strength is zero.");
				this->ZERO_FIELD_CASE = true;
			}

			if (p.T_K == 0) {
				mb::normal_statement("Notice: gas temperature is zero, using cold-case.");
				this->COLD_CASE = true;
				this->Np = 101325.0 / mb::KB / (300.0) * (760.0 / p.p_Torr); // just to use a real finite value
			}
			else {
				this->Np = 101325.0 / 1.38E-23 / p.T_K * (760.0 / p.p_Torr);
			}

			this->E0 = this->Np * p.EN_Td * 1E-21; // electric field in V/m
				
			// -- Done with setup, move to calculation
			execute();


			calculation_time = timer.toc();
			timer = arma::wall_clock(); // Turn off the timer by blipping it

			// Now that we are finished, shovel in calculations to output
			this->out = mb::BoltzmannOutput();
			carve_output();
			fill_output();

			mb::normal_statement("Concluded a MultiBolt calculation.");
			
			if (mb::mbSpeaker.get_outOp() == mb::OutputOption::DEBUG) {
				mb::debug_statement("With output:");
				this->out.print();
			}

			return;

		}


	private:

		void execute();

		void check_validity();

		void carve_output(); // output struct instantiation, sizing
		void fill_output(); // output struct filling

		void solve_f0_SST();
		void gov_eq_f0_SST(arma::sword ell, arma::SpMat<double>& A);

		void solve_f0_HD();
		void gov_eq_f0_HD(arma::sword ell, arma::SpMat<double>& A);

		void solve_f1L();
		void gov_eq_f1L(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f1T();
		void gov_eq_f1T(arma::sword ell, arma::SpMat<double>& A, arma::Col<double>& b);

		void solve_f2L();
		void gov_eq_f2L(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);

		void solve_f2T();
		void gov_eq_f2T(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);


		// -- Collision operators
		void C_ela(arma::sword ell, arma::SpMat<double>& A);
		void C_exc(arma::sword ell, arma::SpMat<double>& A);
		void C_att(arma::sword ell, arma::SpMat<double>& A);
		void C_iz(arma::sword ell, arma::SpMat<double>& A);
		void C_sup(arma::sword ell, arma::SpMat<double>& A);
		void full_collision_operator(arma::sword ell, arma::SpMat<double>& A);


		// R = C[f], as in, 
		// BE == Rin - Rout
		// maybe do something with this form later
		arma::colvec C_in_att(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_att(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_ela(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_ela(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_eff(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_eff(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_exc(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_exc(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_sup(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_sup(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_in_iz(const arma::sword ell, const arma::colvec& sigma, const double frac);
		arma::colvec C_out_iz(const arma::sword ell, const arma::colvec& sigma, const double frac);


		void boundary_conditions(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b);
		
		
		void set_grid(); // Act on Grid container based on settings


		// -- Calculate-relevant functions

		arma::colvec calculate_s_Te_eff(); // total xsec weighted by gas-fractions

		double calculate_rate(const arma::uword i_x); // for any rate coefficient
		double calculate_rate(const std::shared_ptr<lib::AbstractXsec>& x);
		double calculate_total_rate(const arma::uvec i_x); // pass in g.i_elastic, g.i_excitation, etc 
		double calculate_total_S(const arma::colvec& f, const arma::uvec i_x); // more generalized form of the above, useful for higher order schemes

		double calculate_growth(const arma::uword i_x);
		double calculate_growth(const std::shared_ptr<lib::AbstractXsec>& x);
		double calculate_total_growth(const arma::uvec i_x);
		
		double calculate_k_iz_eff_N(); 

		double calculate_W_f0();
		double calculate_W_BULK();
		double calculate_energy_W_f0();		// 04/2023

		double calculate_muN_f0();
		double calculate_muN_BULK();
		double calculate_energy_muN_f0();	// 04/2023

		double calculate_avg_en();

		double calculate_alpha_eff_N();

		double calculate_DFTN();
		double calculate_DTN_BULK();

		double calculate_DFLN();
		double calculate_DLN_BULK();

		double calculate_DN_f0();
		double calculate_DN_f0_nu();
		double calculate_energy_DN_f0();	// 04/2023
		double calculate_energy_DN_f0_nu();	// 04/2023


		double calculate_DT_mu();
		double calculate_DL_mu();
		double calculate_D_mu();

		
		// total inelastic xsec for the species
		double species_total_inelastic(const double eV, const arma::uword i_spec);
		arma::colvec species_total_inelastic(const arma::colvec& eV, const arma::uword i_spec);


		// guess grid based on field or temp-driven conditions
		double guess_eV_max();


		mb::RemapChoice check_grid(); // checking related to energy remap
		void lower_grid();
		void raise_grid_little();
		void raise_grid_extrap();
			

		bool check_is_solution_failing(int level);

		double  Du_at_u(const double u);
		arma::colvec  Du_at_u(const arma::colvec& u);

		public:
			arma::colvec calculate_MaxwellBoltzmann_EEDF(); // a courtesy: the limit case of the EEDF with no electron-neutral collisions in hot temperature
			// above is not applicable for cold case
			// important: this is *not* the same thing as the EEDF at thermal equilibrium in any real gas 


			double check_f0_normalization();
		
	public:

		// -- Add getters as necessary for your needs

		mb::BoltzmannOutput get_output()
		{
			return out;
		}
		mb::BoltzmannParameters get_parameters() {
			return p;
		}

		arma::colvec get_x_f() { // useful for time-dependent solvers
			return x_f;
		}

		arma::sp_mat get_scattering_matrix() { // useful for time-dependent solvers ?
			return A_scattering;
		}
};


//// Boundary conditions
/// A note: schemes beyond _0 or SST are insensitive to boundary conditions
//// (odd BC is subtly incorporated in the discretization scheme and does not require an explicit fixed point)
void mb::BoltzmannSolver::boundary_conditions(arma::sword ell, arma::SpMat<double>& A, arma::colvec& b) {
	
	if (ell % 2 == 0) { //% ell = (even), setup even BC

		// f^ l(u = u_max) = 0; for l = (even)

		arma::sword ROW = ell * p.Nu + (p.Nu - 1);
		arma::sword COL = ROW;

		A.row(ROW).zeros();
		A(ROW, COL) = 1.0;
		b.at(ROW) = 0.0;
	}
	else { // if is odd instead // <- do not add in, unnecessary

	}

	return;
};


// Decision-maker while p.USE_EV_MAX_GUESS = true
// Empirical! based on personal-experience
// Distribution is either field-driven (~Druyvesteyn)
// or temperature-driven (~Maxwell-Boltzmann)
// For the constant-ela case, these can be solved exactly
//
// TODO: Does this whiff for very high E/N?
double mb::BoltzmannSolver::guess_eV_max() {

	double AVG_CONSTANT_S = 0;
	double AVG_MRATIO = 0;

	arma::colvec rough_eV = arma::logspace(-2, 2, 100);


	for (auto& spec : this->lib.allspecies) {
		for (auto& x : spec->ela) {
			AVG_CONSTANT_S += spec->frac() * arma::as_scalar(arma::mean(x->eval_at_eV(rough_eV))) / spec->n_ela(); 
			AVG_MRATIO += spec->frac() * x->Mratio() / spec->n_ela();
		}

		for (auto& x : spec->eff) {
			AVG_CONSTANT_S += spec->frac() * arma::as_scalar(arma::mean(x->eval_at_eV(rough_eV))) / spec->n_eff(); // dont care about inel contribution
			AVG_MRATIO += spec->frac() * x->Mratio() / spec->n_eff();
		}
	}

	// choose which is the driver based on which has the larger average value

	double eV_max_Druy = 10.0 * p.EN_Td;

	arma::colvec eV_Druy = arma::linspace(0, eV_max_Druy, 500); // for evaluating dists
	arma::colvec f_Druy = arma::exp(-arma::pow(2.0 * eV_Druy * mb::QE / mb::ME, 2.0) /
		(4.0 / 3.0 / AVG_MRATIO * pow(mb::QE / mb::ME * p.EN_Td * 1e-21 / AVG_CONSTANT_S, 2.0)));
	f_Druy = f_Druy / arma::as_scalar(trapz(eV_Druy, sqrt(eV_Druy) % f_Druy));

	double Druy_avg_en = arma::as_scalar(trapz(eV_Druy, pow(eV_Druy, 1.5) % f_Druy));


	double eV_max_MB = 100.0 * mb::KB * p.T_K / mb::QE;

	arma::colvec eV_MB = arma::linspace(0, eV_max_MB, 500); // for evaluating dists
	arma::colvec f_MB = arma::exp(-eV_MB * mb::QE / (mb::KB * p.T_K));
	f_MB = f_MB / arma::as_scalar(trapz(eV_MB, sqrt(eV_MB) % f_MB));

	double MB_avg_en = arma::as_scalar(trapz(eV_MB, pow(eV_MB, 1.5) % f_MB));
	

	bool IS_FIELD_DRIVEN = false;
	if (p.T_K == 0) {
		IS_FIELD_DRIVEN = true;
	}
	else if (p.EN_Td == 0) {
		IS_FIELD_DRIVEN = false;
	}
	else {
		IS_FIELD_DRIVEN = Druy_avg_en > MB_avg_en;
	}


	double solution = 0;

	if (IS_FIELD_DRIVEN) {

		double log_height = abs(log10(f_Druy(0)) - log10(f_Druy(f_Druy.n_elem - 1)));

		if (log_height < 10.0) {
			solution=eV_max_Druy; // extrapolation is not worth it just for the guess
		}
		else {
		
			// interpolate to find point
			arma::colvec sol;
			arma::colvec probe = { 10.0 };

			arma::colvec decay = log10(f_Druy.at(0)) - log10(f_Druy);
			interp1(decay, eV_Druy, probe, sol);
			
			solution = sol.at(0);
		}

		// TODO: maybe this is the culprit
		if (p.EN_Td < solution) {
			solution = std::min(p.EN_Td, Druy_avg_en/2.0);	// grids at very large eV might be given by the Druy if EN_Td is large
															// but at this point, swarm is very likely inelastic-driven
		}
	}
	else {
		double log_height = abs(log10(f_MB(0)) - log10(f_MB(f_MB.n_elem - 1)));

		if (log_height < p.remap_target_order_span) {
			solution = eV_max_MB; // extrapolation is not worth it just for the guess
		}
		else {

			// interpolate to find point
			arma::colvec sol;
			arma::colvec probe = { 10.0 };

			arma::colvec decay = log10(f_MB.at(0)) - log10(f_MB);
			
			interp1(decay, eV_MB, probe, sol);

			solution = sol.at(0);
		}
	}

	if (!std::isfinite(solution)) {
		solution = std::min(p.EN_Td, 100.0);
		solution = std::max(p.EN_Td, 1.0);
	}

	return solution;
};