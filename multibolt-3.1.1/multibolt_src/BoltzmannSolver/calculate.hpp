// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"



// The purpose of this file is to hold the functions which are used to calculate all the things
// that MultiBolt cares about


// Some notes:
// "flux" transport coefficients generally describe transport of individuals solely due to the external forces
// "bulk" transport coefficients, however, describe transport of the center of mass
// (which is different in HD swarm descriptions when non-conservative collisions occur)




// -- Calculate electron particle drift velocity -----------------------------------------------------------------------------


// Note: drift velocities are *always* calculated in the form of w propto intergral(eV * f1, deV)


// Electron particle drift velocity based on the 0th order (isotropy) from a GE perspective
// for SST cases, this is equivalent to W_f0_SST
// for HD cases, this is equivalent to W_FLUX
double mb::BoltzmannSolver::calculate_W_f0() {

	// TODO: confusing naming convention

	double temp = 1.0 / 3.0 * GAMMA_eV * arma::accu(trapz(g.eV_o, g.eV_o % x_f(g.ell1_idx)));
	return temp;
}


// Bulk electron particle drift velocity via gradient descent
// Differs from (flux) above when nonconservative collisions are present
double mb::BoltzmannSolver::calculate_W_BULK() {

	if (p.model != mb::ModelCode::HDGE && p.model != mb::ModelCode::HDGE_01) {
		return arma::datum::nan;
	}

	double Gamma_f1L = calculate_W_f0();
	double S_1L = (calculate_total_S(x_f1L(g.ell0_idx), g.i_ionization) - calculate_total_S(x_f1L(g.ell0_idx), g.i_attachment));
	omega1 = Gamma_f1L + S_1L;

	return omega1;
}

// 04/2023
// Calculate energy W
// Assuming analogous to W_FLUX for particles
double mb::BoltzmannSolver::calculate_energy_W_f0() {

	// TODO: confusing naming convention

	// extra factor of energy in integrand
	double temp = (1.0 / 3.0) * GAMMA_eV / (calculate_avg_en()) * arma::accu(trapz(g.eV_o, g.eV_o % g.eV_o % x_f(g.ell1_idx)));
	return temp;
}


// -- Calculate mobility coefficients -------------------------------------------------------------------------------------

// Mobility coefficient for electron particle transport using the strict W = mu * E; W = muN / (E/N) defintion
// for SST cases, this is equivalent to muN_SST
// for HD/HD+GE cases, this is equivalent to muN_FLUX
double mb::BoltzmannSolver::calculate_muN_f0() {
	return calculate_W_f0() / (p.EN_Td * 1e-21);
}

// Bulk mobility coefficient for electron particle transport
double mb::BoltzmannSolver::calculate_muN_BULK() {
	return (p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) ? 
		calculate_W_BULK() / (p.EN_Td * 1e-21) : arma::datum::nan;
}

// 04/2023
// Mobility coefficient for energy transport
double mb::BoltzmannSolver::calculate_energy_muN_f0() {

	// using W = muN * E/N but analagous for energy instead
	return calculate_energy_W_f0() / (p.EN_Td * 1e-21);
}



// -- Calculate energies of the electron swarm -------------------------------------------------------------------------


// Mean electron energy of swarm
double mb::BoltzmannSolver::calculate_avg_en() {

	return arma::accu(trapz(g.eV_e, arma::pow(g.eV_e, 1.5) % x_f(g.ell0_idx)));
}


// The following are characteristic energies,
// to be honest the distinction of what you'd rather use and where is a bit unclear
// Experiments usually report DT/mu or DL/mu specifically. Probably want to include nonconservative effects.

double mb::BoltzmannSolver::calculate_DT_mu() {
	return (p.model == mb::ModelCode::HDGE) ? calculate_DTN_BULK() / calculate_muN_BULK() : arma::datum::nan;
}

double mb::BoltzmannSolver::calculate_DL_mu() {
	return (p.model == mb::ModelCode::HDGE) ? calculate_DLN_BULK() / calculate_muN_BULK() : arma::datum::nan;
}

double mb::BoltzmannSolver::calculate_D_mu() {
	return calculate_DN_f0() / calculate_muN_f0(); // todo: check that you're using the right DN
}



// -- Calculate electron particle spatial number growth ----------------------------------------------------------------------------


// Primary Townsend coefficient
// Definition depends on model in-use
double mb::BoltzmannSolver::calculate_alpha_eff_N() {

	double val = arma::datum::nan;

	if (p.model == mb::ModelCode::HDGE) {
		
		double W = calculate_W_BULK();
		double k_N = calculate_k_iz_eff_N();
		double DLN = calculate_DLN_BULK();

		val = W / (2.0 * DLN) - sqrt(pow(W / (2.0 * DLN), 2.0) - k_N / DLN);
	}
	else if (p.model == mb::ModelCode::HD || p.model == mb::ModelCode::HDGE_01) {
		val = arma::datum::nan; // HD models require GE in order to actually *know* this
								// k/W would be a first order approximation. In interest of not confusing anybody, leaving this as nan.
	}
	else if (p.model == mb::ModelCode::SST){

		val = calculate_k_iz_eff_N() / calculate_W_f0();
	}

	return val;
}

// Calculate an individual growth coefficient
// I supposed this could also be thought of a "events per space traveled" for conservative collisions
double mb::BoltzmannSolver::calculate_growth(const arma::uword i_x) {

	if (p.model != mb::ModelCode::SST) {
		return arma::datum::nan; // it is unclear if individual growth coefficients can be discerned out of HDGE
	}
	return calculate_rate(i_x) / calculate_W_f0();
}
double mb::BoltzmannSolver::calculate_growth(const std::shared_ptr<lib::AbstractXsec>& x) {

	if (p.model != mb::ModelCode::SST) {
		return arma::datum::nan; // it is unclear if individual growth coefficients can be discerned out of HDGE
	}
	return calculate_rate(x) / calculate_W_f0();
}


double mb::BoltzmannSolver::calculate_total_growth(const arma::uvec i_x) {

	if (p.model != mb::ModelCode::SST) {
		return arma::datum::nan; // it is unclear if individual growth coefficients can be discerned out of HDGE
	}
	return calculate_total_rate(i_x) / calculate_W_f0();
}


// -- Calculate electron collision reaction rates ----------------------------------------------------------------------------------------


// Calculate an individual rate coefficient k/N
double mb::BoltzmannSolver::calculate_rate(const arma::uword i_x) {
		
	// frac dependence removed at this stage - meaning, no rate is zero even for species with frac = 0.0
	return GAMMA_eV * arma::accu(arma::trapz(g.eV_e, g.sigma_e.col(i_x) % g.eV_e % x_f(g.ell0_idx)));
}
double mb::BoltzmannSolver::calculate_rate(const std::shared_ptr<lib::AbstractXsec>& x) {

	// frac dependence removed at this stage - meaning, no rate is zero even for species with frac = 0.0
	return GAMMA_eV * arma::accu(arma::trapz(g.eV_e, x->eval_at_eV(g.eV_e, p.interp_method) % g.eV_e % x_f(g.ell0_idx)));
}

// Calculate an total rate k/N
double mb::BoltzmannSolver::calculate_total_rate(const arma::uvec i_x) {

	double val = 0;
	for (auto i : i_x) {
		val += calculate_rate(i) * g.i_frac(i);
	}

	return val;
}



// More generalized form of calculating a rate
double mb::BoltzmannSolver::calculate_total_S(const arma::colvec& f, const arma::uvec i_x) {

	double val = 0;

	for (auto i : i_x) {
		val +=  GAMMA_eV * arma::accu(arma::trapz(g.eV_e, g.i_frac(i) * g.sigma_e.col(i) % g.eV_e % f));	
	}
	
	return val;
}

// Effective ionization rate coefficient (equiv to omega0 for f0 scheme)
double mb::BoltzmannSolver::calculate_k_iz_eff_N() {
	return (calculate_total_rate(g.i_ionization) - calculate_total_rate(g.i_attachment));
}


// -- Calculate diffusion coefficients ---------------------------------------------------------------------------------


// Flux transverse diffusion coefficient for electron particle transport
double mb::BoltzmannSolver::calculate_DFTN() {

	return (p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) ?
		(1.0 / 3.0) * GAMMA_eV * arma::accu(trapz(g.eV_o, g.eV_o % x_f1T(g.ell1_idx))) : arma::datum::nan;  // Flux portion of DTN
} // ?

// Flux longitudinal diffusion coefficient for electron particle transport
double mb::BoltzmannSolver::calculate_DFLN() {
	return (p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) ?
		(1.0 / 3.0) * GAMMA_eV * arma::accu(trapz(g.eV_o, g.eV_o % x_f1L(g.ell1_idx))) : arma::datum::nan; // Flux portion of DLN
}


// Bulk longitudinal diffusion coefficient for electron particle transport
double mb::BoltzmannSolver::calculate_DLN_BULK() {
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / sqrt(3.0)) * (omega2 - sqrt(2.0) * omega2_bar) * Np : arma::datum::nan;
}

// Bulk transverse diffusion coefficient for electron particle transport
double mb::BoltzmannSolver::calculate_DTN_BULK() {
	return (p.model == mb::ModelCode::HDGE) ?
		(1.0 / sqrt(3.0)) * (omega2 + 1.0 / sqrt(2.0) * omega2_bar) * Np : arma::datum::nan;
}



// "Conventional" isotropic diffusion coefficient for electron particle transport (conservative only)
// See eq. (56) of Hagelaar and Pitchford 2005, doi: 10.1088/0963-0252/14/4/011
// (equiv to when sigm_m_tilde == sigma_m because no iz/att exists)
// Corrected for multi-term
double mb::BoltzmannSolver::calculate_DN_f0() {

	// TODO: clearly should be changed to be named something like "DN_f0_conservative"

	arma::colvec sigma_m = calculate_s_Te_eff();

	
	arma::colvec f;
	if (p.N_terms >= 3) {
		f = x_f(g.ell0_idx) + 2.0 / 5.0 * x_f(g.ell2_idx);
	}
	else {
		f = x_f(g.ell0_idx);
	}

	return (1.0 / 3.0) * GAMMA_eV * arma::accu(trapz(g.eV_e , g.eV_e / sigma_m % f));
}


// "Conventional" isotropic diffusion coefficient for electron particle transport (including nonconservative effects)
// Note: this used to be called the "conventional" diffusion coefficient for neutrals for some reason. It's not. No idea where that idea came from, sorry.
// See eq. (56) of Hagelaar and Pitchford 2005, doi: 10.1088/0963-0252/14/4/011
// Corrected for multi-term
double mb::BoltzmannSolver::calculate_DN_f0_nu() {

	// TODO: really should change this name to be more similar to Hagelaar and Pitchford labeling.

	arma::colvec sigma_m_tilde = (calculate_s_Te_eff() + calculate_k_iz_eff_N() * (1.0 / (g.eV_e * GAMMA_eV )));
	
	arma::colvec f;
	if (p.N_terms >= 3) {
		f = x_f(g.ell0_idx) + 2.0 / 5.0 * x_f(g.ell2_idx);
	}
	else {
		f = x_f(g.ell0_idx);
	}
	
	return (1.0 / 3.0) * GAMMA_eV * arma::accu(trapz(g.eV_e , g.eV_e / sigma_m_tilde % f));
}

// 04/2023
// "Conventional" isotropic diffusion coefficient for energy transport (not including nonconservative effects)
// See eq. (62) of Hagelaar and Pitchford 2005, doi: 10.1088/0963-0252/14/4/011
// Corrected for multi-term
double mb::BoltzmannSolver::calculate_energy_DN_f0() {


	arma::colvec sigma_m_tilde = calculate_s_Te_eff();

	arma::colvec f;
	if (p.N_terms >= 3) {
		f = x_f(g.ell0_idx) + 2.0 / 5.0 * x_f(g.ell2_idx);
	}
	else {
		f = x_f(g.ell0_idx);
	}

	// extra factor of energy in integral, extra factor of mean energy out front
	return (1.0 / 3.0) * (1.0 / calculate_avg_en()) * GAMMA_eV * arma::accu(trapz(g.eV_e, g.eV_e % (g.eV_e / sigma_m_tilde) % f));

};


// 04/2023
// "Conventional" isotropic diffusion coefficient for energy transport (including nonconservative effects)
// See eq. (62) of Hagelaar and Pitchford 2005, doi: 10.1088/0963-0252/14/4/011
// Corrected for multi-term
double mb::BoltzmannSolver::calculate_energy_DN_f0_nu(){


	arma::colvec sigma_m_tilde = (calculate_s_Te_eff() + calculate_k_iz_eff_N() * (1.0 / (g.eV_e * GAMMA_eV)));

	arma::colvec f;
	if (p.N_terms >= 3) {
		f = x_f(g.ell0_idx) + 2.0 / 5.0 * x_f(g.ell2_idx);
	}
	else {
		f = x_f(g.ell0_idx);
	}

	// extra factor of energy in integral, extra factor of mean energy out front
	return (1.0 / 3.0) * (1.0 / calculate_avg_en()) * GAMMA_eV * arma::accu(trapz(g.eV_e,  g.eV_e % (g.eV_e / sigma_m_tilde) % f));
};




// -- Calculate cross-section totals and such -------------------------------------------------------------------------------------------

// Total cross-section of the whole system, weighted by gas-fractions
arma::colvec mb::BoltzmannSolver::calculate_s_Te_eff() {
	
	arma::colvec temp(p.Nu, arma::fill::zeros);
	for (auto i_x : g.i_collision) {
		temp += g.sigma_e.col(i_x) * g.i_frac(i_x);
	}

	return temp;
}



arma::colvec mb::BoltzmannSolver::species_total_inelastic(const arma::colvec& eV, const arma::uword i_spec) {
	arma::colvec temp(eV.n_elem, arma::fill::zeros);

	arma::uvec these_x = arma::find(g.i_collision_species == i_spec);

	for (auto i_x : these_x) {
		auto x = g.xsec_ptr(i_x);

		if (!(x->code() == lib::CollisionCode::excitation) && (x->code() == lib::CollisionCode::superelastic)) {
			continue;
		}
		temp += x->eval_at_eV(eV, p.interp_method);
	}
	return temp;
};
double mb::BoltzmannSolver::species_total_inelastic(const double eV, const arma::uword i_spec) {
	double temp = species_total_inelastic({ eV }, i_spec);
	return temp; 
};




// -- Other miscellaneous functions -----------------------------------------------------------------------------------------------------

// EEDF proportional to sqrt(e)*exp(-e/(kB*T)) while EN_Td=zero and T > 0 K
arma::colvec mb::BoltzmannSolver::calculate_MaxwellBoltzmann_EEDF() {

	arma::colvec EEDF(p.Nu, arma::fill::zeros);
	if (COLD_CASE) {
		EEDF.fill(arma::datum::nan);
	}
	else {
		EEDF = pow(mb::ME / (2.0 * mb::PI * (mb::KB * p.T_K / mb::QE)), 1.5)  * 
			arma::exp( - g.eV_e / (mb::KB * p.T_K / mb::QE)) * sqrt(2.0 * mb::ME) / pow(mb::ME, 2.0) * 4.0 * mb::PI;
	}
	return EEDF;
}



