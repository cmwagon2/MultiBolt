// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#include "multibolt"

// -- Perform MultiBolt solution 
void mb::BoltzmannSolver::execute() {

	this->x_f = arma::colvec(p.Nu * p.N_terms, arma::fill::zeros);
	
	set_grid(); // initial grid-set. Also fills A_scattering. These two only change on the f0 level if the grid is allowed to update.
		

	for (int i = 0; i < p.remap_grid_trial_max; ++i) {

		if (p.model == mb::ModelCode::HD || p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) {
			solve_f0_HD();
		}
		else if (p.model == mb::ModelCode::SST) {
			solve_f0_SST();
		}

		if (i == (p.remap_grid_trial_max - 1)) {
			mb::normal_statement("Grid trial maximum reached, no more will be tested.");
		
			if (!this->solution_succesful) {
				return; // exit out because results beyond this will be unrealistic
			}
			break;
		}


		if (p.USE_ENERGY_REMAP == true ) {
			mb::RemapChoice op = check_grid();
			if (op == mb::RemapChoice::LowerGrid) {
				mb::normal_statement("Tail of f0 looks small; grid maximum is being lowered. f0 will be solved again.");
				lower_grid();

			}
			else if (op == mb::RemapChoice::RaiseGridExtrap) {
				mb::normal_statement("Tail of f0 looks large; grid minimum is being raised significantly. f0 will be solved again.");
				raise_grid_extrap();
			}
			else if (op == mb::RemapChoice::RaiseGridLittle) {
				mb::normal_statement("Tail of f0 looks large; grid minimum is being raised a bit. f0 will be solved again.");
				raise_grid_extrap();
			}
			else {
				if (!this->solution_succesful) {
					return; // exit out because results beyond this will be unrealistic
				}
				break; // grid is fine, can be left alone
			}
		}
		else {
			if (!this->solution_succesful) {
				return; // exit out because results beyond this will be unrealistic
			}
			break; // don't fuss with the grid, leave
		}
	
	}


	// No particular reason you can't use GE with only 2 terms
	// But it tends to whiff on the diffusion coefficients
	if (p.model == mb::ModelCode::HDGE || p.model == mb::ModelCode::HDGE_01) {

		this->x_f1L = arma::colvec(p.Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f1T = arma::colvec(p.Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f2L = arma::colvec(p.Nu * p.N_terms, arma::fill::value(arma::datum::nan));
		this->x_f2T = arma::colvec(p.Nu * p.N_terms, arma::fill::value(arma::datum::nan));

	#ifdef MULTIBOLT_USING_OPENMP

		#pragma omp parallel sections
		{

			#pragma omp section
			solve_f1T();

			#pragma omp section
			solve_f1L();
		}

		if (!this->solution_succesful) {
			return; // exit out because results beyond this will be unrealistic
		}


		if (p.model == mb::ModelCode::HDGE) {
			// Calculate second set of gradient expansion systems
			
			#pragma omp parallel sections
			{

				#pragma omp section
				solve_f2T();

				#pragma omp section
				solve_f2L();

			}

			if (!this->solution_succesful) {
				return; // exit out because results beyond this will be unrealistic
			}
		}

	#else

		// totally without openmp.
		solve_f1T();
		solve_f1L();

		if (!this->solution_succesful) {
			return; // exit out because results beyond this will be unrealistic
		}

		if (p.model == mb::ModelCode::HDGE) {
			solve_f2T();
			solve_f2L();
		}

		if (!this->solution_succesful) {
			return; // exit out because results beyond this will be unrealistic
		}

	#endif


	}

}