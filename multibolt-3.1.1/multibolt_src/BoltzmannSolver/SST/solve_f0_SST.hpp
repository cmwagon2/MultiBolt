// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2021-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 
#pragma once

#include "multibolt"


// Solve the f0 governing equation for steady state Townsend conditions
// this is not the same equation as for HD conditions
void mb::BoltzmannSolver::solve_f0_SST() {




	
	mb::debug_statement("Begin solving governing equation for f0 (SST).");

	// initialize things that can be found here
	this->alpha_eff_N = 0;

	double avg_en = 0;
	
	double W_f0_SST = 0;
	double k_iz_eff_N = 0;



	arma::sword N = p.Nu * p.N_terms;

	arma::Col<double> b(N, arma::fill::zeros); // solve-against vector (contains normalization condition)
	arma::Col<double> x(N, arma::fill::zeros); // solution vector (contains distribution functions)
	arma::SpMat<double> A(N, N); // coefficient matrix

	// initialize "previous values"
	double alpha_eff_N_prev = 0; // previous value, tracking for convergence
	double avg_en_prev = 0;

	bool CONVERGED = false;

	for (iter_f0_SST = 1;  iter_f0_SST <= p.iter_min || (iter_f0_SST < p.iter_max && !CONVERGED); ++iter_f0_SST) {

		A.zeros();
		b.zeros();

		for(arma::sword ell = 0; ell < p.N_terms; ++ell) {
			gov_eq_f0_SST(ell, A);
		}

		A = A - A_scattering;


	

		// Normalization condition
		A.row(0).zeros();
		A.head_cols(p.Nu).row(0) = (sqrt(g.u_e / mb::QE) % Du_at_u(g.u_e) / mb::QE).t();
		b.at(0) = 1.0;


		//// Boundary conditions
		for (arma::sword ell = 1; ell < p.N_terms; ++ell) {
			boundary_conditions(ell, A, b);
		}

		
		bool solve_success = arma::spsolve(x_f, A, b, "superlu", multibolt_superlu_opts()); // solve
		if (!solve_success) {
			mb::error_statement("Solution failing: spsolve unsuccesful.");
			this->solution_succesful = false;
			return;
		}
		


		avg_en_prev = avg_en;
		avg_en = calculate_avg_en();

		alpha_eff_N_prev = alpha_eff_N;
		alpha_eff_N = (1.0 - present_weight_f0) * (alpha_eff_N_prev) + (present_weight_f0) * (calculate_alpha_eff_N());

		// initial check: is the eedf wildly wrong?
		if (iter_f0_HD > p.iter_min) {

			double fnorm = check_f0_normalization();
			if (fnorm > 1.0 || fnorm < 0.0) { // unstable solutions tend to occasionally think fnorm is impossible

				present_weight_f0 = present_weight_f0 / 2.0;

				mb::normal_statement("EEDF normalization fails, EEDF may be poorly-behaved.");

				if (present_weight_f0 > 1e-4) {

					mb::normal_statement("Attempting to save solution by reducing weight_f0.");


					present_weight_f0 = std::max(present_weight_f0, 1e-4);
					mb::normal_statement("weight_f0 is now: " + mb::mb_format(present_weight_f0) + ".");

					alpha_eff_N_prev = 0;
					alpha_eff_N = 0;
					avg_en = 0;
					avg_en_prev = 0;

					iter_f0_HD = 1;

					continue;
				}



			}
		}
		


		// check if ionization is ocurring at all - converge against avg_en instead if need be
		if (this->NO_ITERATION) {
			mb::normal_statement("Single iteration case: no ionization or attachment found.");
			mb::display_iteration_banner("fSST", iter_f0_SST, "avg_en [eV]", avg_en, 0);
			CONVERGED = true;
			break;
		}
		else {
			mb::display_iteration_banner("fSST", iter_f0_SST, "avg_en [eV]", avg_en, err(avg_en, avg_en_prev));
			CONVERGED = mb::converged(p.conv_err, avg_en, avg_en_prev);

			//mb::display_iteration_banner("fSST", iter_f0_SST, "alpha_eff_N [m**2]", alpha_eff_N, err(alpha_eff_N, alpha_eff_N_prev));
			//CONVERGED = mb::converged(p.conv_err, alpha_eff_N, alpha_eff_N_prev);
		}

		// extra check: assume that if you are past iter_min, you're willing to let SHYness boot the solution out early
		if (iter_f0_SST > p.iter_min) {
			if (this->check_is_solution_failing(0) == true) {
				return;
			}
		}
	}

	mb::check_did_not_converge("fSST", iter_f0_SST, p.iter_max);


	mb::debug_statement("Exit solving governing equation for f0 (SST).");
	
	
}



