cmake_minimum_required(VERSION ${CMAKE_MINIMUM_REQUIRED_VERSION})

get_filename_component(ProjectId ${CMAKE_CURRENT_SOURCE_DIR} NAME)
string(REPLACE " " "_" ProjectId ${ProjectId})
project(${ProjectId})


add_subdirectory(HDGE_Conservative)
add_subdirectory(HDGE_NonConservative)
add_subdirectory(SST)
add_subdirectory(scattering_Conservative)
add_subdirectory(scattering_NonConservative)
add_subdirectory(temperature)
add_subdirectory(field)
