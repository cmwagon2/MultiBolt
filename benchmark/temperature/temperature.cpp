// SPDX-License-Identifier: MIT
//
// MultiBolt - a multi-term Boltzmann equation solver for low temperature plasma
// 
// Copyright 2022-2023 Max Flynn, Jacob Stephens
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 


// Source file that checks temperature influence on the gas is working
// Non-zero temperature alongside zero-field should create a maxwellian
// in a constant elastic-only cross section

#include "multibolt"



int main() {


	lib::libSpeaker.printmode_normal_statements();
	mb::mbSpeaker.printmode_normal_statements();



	/* Develop library of analytic species -------------------------------------------------------- */

	lib::Library lib = lib::get_ReidRamp_Library();
	
	// get rid of excited-state for this case
	lib.erase_by_index(1);
	lib.allspecies[0]->erase_by_index(1); // get rid of the neutral excitation
	
	/* Set up parameters for solver --------------------------------------------------------*/

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HD;
	p.N_terms = 2; // zero field actually should all be in ell=0

	p.Nu = 1000;	

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. 


	p.initial_eV_max = 0.5;

	p.EN_Td = 0;
	p.p_Torr = 760;

	arma::colvec T_vec = { 77, 300, 3000 };
	p.T_K = T_vec(0);

	p.sharing = 0.5; // energy sharing between primary and secondary electrons in ionization

	p.USE_ENERGY_REMAP = true;
	p.USE_EV_MAX_GUESS = true;

	std::vector<mb::BoltzmannSolver> run_vec(T_vec.size(), mb::BoltzmannSolver());
	std::vector<mb::BoltzmannOutput> sols_vec(T_vec.size(), mb::BoltzmannOutput());

	/* Perform Boltzmann solver calculation  */

	// instantiate and perform BE solutions
	#pragma omp parallel
	#pragma omp for
	for (int i = 0; i < T_vec.size(); ++i) {

		p.T_K = T_vec(i);

		run_vec.at(i) = mb::BoltzmannSolver(p, lib);
		sols_vec.at(i) = run_vec.at(i).get_output();

	}

	for (int i = 0; i < T_vec.size(); ++i) {


		printf("\nGas temp: %2.2f Kelvin", T_vec.at(i));

		auto eV = sols_vec.at(i).eV_even;
		auto calc_maxwellBoltzmann = sols_vec.at(i).f0;
		auto exact_maxwellBoltzmann = run_vec.at(i).calculate_MaxwellBoltzmann_EEDF();

		// Mean electron energy
		double elem_calc = arma::as_scalar(trapz(eV, pow(eV, 1.5) % calc_maxwellBoltzmann));
		double elem_exact = arma::as_scalar(trapz(eV, pow(eV, 1.5) % exact_maxwellBoltzmann));;


		double err1 = (elem_calc - elem_exact) / abs(elem_calc);
		printf("\n\tMean Energy :\t\tMB: %1.4e\t\tExact: %1.4e\t\t\t%1.4e%%", elem_calc, elem_exact, err1 * 100);

	}


	
	/*
	* 
	* //double ELA_VAL = lib.allspecies[0]->ela[0]->eval_at_eV(0.01);

	// added more meaningful comparison values

	std::cout << "Calculated: " << std::endl;
	std::cout << "Norm: " << arma::as_scalar(trapz(out.eV_even, sqrt(out.eV_even) % calc_maxwellBoltzmann)) << std::endl;
	std::cout << "Mean energy: " << arma::as_scalar(trapz(out.eV_even, pow(out.eV_even, 1.5) % calc_maxwellBoltzmann)) << " eV " << std::endl;
	std::cout << "Most-probable energy: " << out.eV_even(arma::index_max(sqrt(out.eV_even) % calc_maxwellBoltzmann)) << " eV" << std::endl; 
	std::cout << "RMS energy: " << sqrt(arma::sum( pow(out.eV_even % calc_maxwellBoltzmann, 2) / p.Nu)) << " eV"<< std::endl;
	//std::cout << "Elastic collision rate " << arma::as_scalar(trapz( out.eV_even, sqrt(2.0*mb::QE/mb::ME ) * ELA_VAL * out.eV_even % calc_maxwellBoltzmann)) << " m^3 s^-1" << std::endl;

	std::cout << std::endl;
	std::cout << "'Exact': " << std::endl;
	std::cout << "Norm: " << arma::as_scalar(trapz(out.eV_even, sqrt(out.eV_even) % maxwellBoltzmann)) << std::endl;
	std::cout << "Mean energy: " << arma::as_scalar(trapz(out.eV_even, pow(out.eV_even, 1.5) % maxwellBoltzmann)) << " eV " << std::endl;
	std::cout << "Most-probable energy: " << out.eV_even(arma::index_max(sqrt(out.eV_even) % maxwellBoltzmann)) << " eV " << std::endl;
	std::cout << "RMS energy: " << sqrt(arma::sum(pow(out.eV_even % maxwellBoltzmann, 2) / p.Nu)) << " eV " << std::endl;
	//std::cout << "Elastic collision rate: " << arma::as_scalar(trapz(out.eV_even, sqrt(2.0 * mb::QE / mb::ME) * ELA_VAL * out.eV_even % maxwellBoltzmann)) << " m^3 s^-1 "<< std::endl;



	for (int i = 0; i < T_vec.size(); ++i) {


		

		double elemmb = RR_HDGE_sols[i].avg_en;
		double elemref1 = White_HDGE_RR_vec[i].avg_en;
		double elemref2 = Raspopovic_HDGE_RR_vec[i].avg_en;

		double err1 = (elemmb - elemref1) / abs(elemmb);
		double err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tavg_en :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);




		elemmb = RR_HDGE_sols[i].W_FLUX;
		elemref1 = White_HDGE_RR_vec[i].W_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].W_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tW_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);



		elemmb = RR_HDGE_sols[i].DTN_FLUX;
		elemref1 = White_HDGE_RR_vec[i].DTN_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].DTN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDTN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		elemmb = RR_HDGE_sols[i].DLN_FLUX;
		elemref1 = White_HDGE_RR_vec[i].DLN_FLUX;
		elemref2 = Raspopovic_HDGE_RR_vec[i].DLN_FLUX;

		err1 = (elemmb - elemref1) / abs(elemmb);
		err2 = (elemmb - elemref2) / abs(elemmb);
		printf("\n\tDLN_FLUX :\t\tMB: %1.4e\t\tMC: %1.4e\t\tMC: %1.4e\t\t%1.4e%%\t\t%1.4e%%", elemmb, elemref1, elemref2, err1 * 100, err2 * 100);


		std::cout << std::endl;
	}
	*/

	


	


	return 0;
}







