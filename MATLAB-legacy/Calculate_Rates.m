% Jacob Stephens, MIT 2018
% -------------------------- MultiBolt_v2.01 ---------------------------- %

% Calculate_Rates    
alpha_N_prev = alpha_N;
k_iz_eff_prev = k_iz_eff;
avg_en_prev = avg_en;
avg_en = trapz(ue/qe,((ue/qe).^1.5).*f_0); % Mean energy [eV]
w = 1/3*sqrt(2/me/qe)*trapz(uo(uvt)/qe,uo(uvt).*f_1(uvt)); % flux portion of the drift velocity
muN_f0 = w/E0*Np;                                          % flux portion of the mobility
DN_f0 = 1/3*sqrt(2/me/qe)*trapz(ue/qe,ue./s_Te_eff.*f_0);  % Conventional expression for diffusion coefficient. Does not account for non-conservative effects.
DN_f0_nu =  1/3*sqrt(2/me/qe)*trapz(ue/qe,ue./(s_Te_eff+k_iz_eff*(1./(ue*(2/qe/me).^(0.5)))).*f_0);

for Ng = 1:N_gases
    k_ela(Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,frac(Ng)*squeeze(s_me(uv,Ng))'.*f_0(uv).*ue(uv).^1.0/qe);
    for i_att = 1:N_atts(Ng)
        k_att(i_att,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,frac(Ng)*squeeze(s_atte(uv,i_att,Ng))'.*f_0(uv).*ue(uv).^1.0/qe);
    end
    for i_exc = 1:N_excs(Ng)
        k_exc(i_exc,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,frac(Ng)*squeeze(s_exce(uv,i_exc,Ng))'.*f_0(uv).*ue(uv).^1.0/qe);
    end
    for i_iz = 1:N_izs(Ng)
        k_iz(i_iz,Ng) = sqrt(2*qe/me)*trapz(ue(uv)/qe,frac(Ng)*squeeze(s_ize(uv,i_iz,Ng))'.*f_0(uv).*ue(uv).^1.0/qe); % Ionization rate based on the current solution
    end
end
k_att_total = sum(sum(k_att));
k_iz_total = sum(sum(k_iz));
k_iz_effNEW = k_iz_total-k_att_total;
k_iz_eff = ((1-fNEW)*k_iz_eff+fNEW*k_iz_effNEW); % Weighted update of the ionization coefficient (these fractions can be adjusted to acheive better convergence)
alpha_N = k_iz_eff/w; % First Townsend coeff./N based on the current solution