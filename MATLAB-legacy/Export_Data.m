% Jacob Stephens, MIT 2018
% -------------------------- MultiBolt_v2.01 ---------------------------- %

% Export_data
fmt = '%5e \t %5e \n';
if isdir('Exported_Data') == 0
    mkdir('Exported_Data')
end
if is == 1
    for i1 = 1:1000
        str_write = ['Exported_Data/Run_',num2str(i1)];
        if isdir(str_write)==0 && flag1_file == 0
            flag1_file = 1;
            RUN_ID = i1;
            mkdir(str_write)
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/RUN_DETAILS.txt'],'a+');
            fprintf(fid,'%s \n \n',['Setup Details for Run ID #',num2str(RUN_ID)]);
            fprintf(fid,'%s\n',['Cross-Section Reference Info']);
            fprintf(fid,'%s\n',['Reference #1']);
            for Ng = 1:N_gases
                if sum(strfind(Biagi_Ref1Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Biagi database, www.lxcat.net, retrieved on June 3, 2018']);
                elseif sum(strfind(Hayashi_Ref1Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Hayashi database, www.lxcat.net, retrieved on June 3, 2018']);       
                else
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Cross-sections not distributed with MultiBolt']);
                    fprintf(fid,'\t\t%s \n',['Consult LXCat Cross-section files for proper reference details']);
                end   
            end
            
            fprintf(fid,'\n%s\n',['Reference #2']);
            for Ng = 1:N_gases
                if sum(strfind(Biagi897_Ref2Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Fortran program, MAGBOLTZ, S.F. Biagi, versions 8.97 and after']);
                elseif sum(strfind(Biagi890_Ref2Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Fortran program, MAGBOLTZ, S.F. Biagi, versions 8.90 and after']);
                elseif sum(strfind(Biagi106_Ref2Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Fortran program, MAGBOLTZ, S.F. Biagi, versions 10.6 and after']);
                elseif sum(strfind(HayashiCaptelli_Ref2_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': M. Hayashi, "Electron collision cross sections determined']);
                    fprintf(fid,'\t\t\t%s \n',                    ['from beam and swarm data by Boltzmann analysis" in ']);
                    fprintf(fid,'\t\t\t%s \n',                    ['"Nonequilibrium Processes in Partially Ionized Gases,"']);
                    fprintf(fid,'\t\t\t%s \n',                    [' eds. M. Capitelli and J.N. Bardsley (Plenum Press, ']);
                    fprintf(fid,'\t\t\t%s \n',                    ['New York, 1990), scanned and digitized by W.L. Morgan, ']);
                    fprintf(fid,'\t\t\t%s \n',                    ['Kinema Research and Software.']);
                elseif sum(strfind(HayashiPitchford_Ref2Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': M. Hayashi, "Electron collision cross sections for molecules']);
                    fprintf(fid,'\t\t\t%s \n',                    ['determined from beam and swarm data," in "Swarm Studies and']);
                    fprintf(fid,'\t\t\t%s \n',                    ['"Nonequilibrium Processes in Partially Ionized Gases,"']);
                    fprintf(fid,'\t\t\t%s \n',                    ['Inelastic Electron-Molecule Collisions," eds. L.C. Pitchford,']);
                    fprintf(fid,'\t\t\t%s \n',                    ['B.V. McKoy, A. Chutjian, and S. Trajmar, (Springer-Verlag,']);
                    fprintf(fid,'\t\t\t%s \n',                    ['New York, 1987) and digitized by S. Chowdhury, 2010']);     
                                                                      
                elseif sum(strfind(HayashiJILA_Ref2Test_string,string(Xsec_fids(Ng)))) ~= 0
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': M. Hayashi, personal communication to the JILA Atomic ']);
                    fprintf(fid,'\t\t\t%s \n',                    ['Collisions Data Center, 1987.']);
                else
                    fprintf(fid,'\t%s \n',['Gas #',num2str(Ng),': Cross-sections not distributed with MultiBolt']);
                    fprintf(fid,'\t\t%s \n',['Consult LXCat Cross-section files for proper reference details']);
                end   
            end

            fprintf(fid,'\n\n%s',['Gas Composition:']);
            for Ng = 1:N_gases
                if Ng ~= 1
                    fprintf(fid,'\n\n\t%s \n',[string(Xsec_fids(Ng))]);
                    fprintf(fid,'\t%s %s %s %s %s \n',['Fraction: ',num2str(frac(Ng),'%.4f'),' |  MASS: ',num2str(mN(Ng)/AMU),' AMU']);
                else
                    fprintf(fid,'\n\t%s \n',[string(Xsec_fids(Ng))]);
                    fprintf(fid,'\t%s %s %s %s %s \n',['Fraction: ',num2str(frac(Ng),'%.4f'),' |  MASS: ',num2str(mN(Ng)/AMU),' AMU']);
                end
            end
            fprintf(fid,'\n\n%s %s',['Nu (Number of energy grid points): ', num2str(Nu)]);
            fprintf(fid,'\n%s %s',['N_TERMS: ', num2str(N_terms)]);
            fprintf(fid,'\n%s %s',['ENERGY REMAP: ', num2str(ENERGY_REMAP)]);
            fprintf(fid,'\n%s %s',['HD: ', num2str(HD)]);
            fprintf(fid,'\n%s %s',['GE: ', num2str(GE)]);
            fprintf(fid,'\n%s %s',['SST: ', num2str(SST)]);
            fprintf(fid,'\n%s %s',['TARGET CONVERGENCE: ', num2str(conv_err,'%10.3e')]);
            fprintf(fid,'\n%s %s %s',['PRESSURE: ', num2str(p_Torr), ' Torr']);
            fprintf(fid,'\n%s %s %s',['NEUTRAL DENSITY: ', num2str(Np,'%10.3e'), ' m^-3']);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/CONVERGENCE_FLAGS.txt'],'a+');
            if GE == 1
                fprintf(fid,'%6s \r %6s \r %6s \r %6s \r %6s \r \n','E/N (Td)','f','f_1L','f_2T','f_2L');
                fprintf(fid,'%5e \r %6.1f \r %6.1f \r %6.1f \r %6.1f \r \n',[EN_Td; converged_f(is); converged_f1L(is); converged_f2T(is); converged_f2L(is)]);
            else
                fprintf(fid,'%6s \r %6s \r \n','E/N (Td)','f');
                fprintf(fid,'%5e \r %6.1f \r \n',[EN_Td; converged_f(is)]);
            end
            fclose(fid);
            
            str_write =  ['Exported_Data/Run_',num2str(RUN_ID),'/EEDFs'];
            mkdir(str_write)
            dlmwrite(['Exported_Data/Run_',num2str(RUN_ID),'/EEDFs/',num2str(EN_Td),'_Td.txt'],[ue/qe; f_0]','delimiter','\t','precision','%5e');
            
            % ------- Output rates for each gas ------- % 
            for Ng = 1:N_gases
                str_write =  ['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng)];
                mkdir(str_write)
                
                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/ELASTIC_RATE.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_ela (m^3/s)');
                fprintf(fid,fmt,[EN_Td; k_ela(Ng)]);
                fclose(fid);

                for i_att = 1:N_atts(Ng)
                    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/ATTACHMENT_RATE_',num2str(i_att),'.txt'],'a+');
                    fprintf(fid,'%12s \t %12s\r\n','E/N (Td)','k_att (m^3/s)');
                    fprintf(fid,fmt,[EN_Td; k_att(i_att,Ng)]);
                    fclose(fid);
                end     
                for i_exc = 1:N_excs(Ng)
                    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/EXCITATION_RATE_',num2str(i_exc),'.txt'],'a+');
                    fprintf(fid,'%12s \t %12s \t %12s \t \n','E/N (Td)','k_exc (m^3/s)',[num2str(u_thresh(N_atts(Ng)+N_ela(Ng)+i_exc,Ng)),' (eV)']);
                    fprintf(fid,fmt,[EN_Td; k_exc(i_exc,Ng)]);
                    fclose(fid);
                end
                for i_iz = 1:N_izs(Ng)
                    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/IONIZATION_RATE_',num2str(i_iz),'.txt'],'a+');
                    fprintf(fid,'%12s \t %12s \t %12s \t \n','E/N (Td)','k_iz (m^3/s)',[num2str(u_thresh(N_atts(Ng)+N_ela(Ng)+N_excs(Ng)+i_iz,Ng)),' (eV)']);
                    fprintf(fid,fmt,[EN_Td; k_iz(i_iz,Ng)]);
                    fclose(fid);
                end                 
            end
            % -------------------------------------------------- % 
            % ------- Output total rates for the mixture ------- % 
            
            str_write =  ['Exported_Data/Run_',num2str(RUN_ID),'/Total'];
            mkdir(str_write)
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/ELASTIC_RATE.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_ela (m^3/s)');
            fprintf(fid,fmt,[EN_Td; sum(k_ela)]);
            fclose(fid);            
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/ATTACHMENT_RATE.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_att (m^3/s)');
            fprintf(fid,fmt,[EN_Td; k_att_total]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/EXCITATION_RATE.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_exc (m^3/s)');
            fprintf(fid,fmt,[EN_Td; sum(sum(k_exc))]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/IONIZATION_RATE.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_iz (m^3/s)');
            fprintf(fid,fmt,[EN_Td; k_iz_total]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/IONIZATION_RATE.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','k_iz (m^3/s)');
            fprintf(fid,fmt,[EN_Td; k_iz_total]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/MEAN_ENERGY.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','mean energy (eV)');
            fprintf(fid,fmt,[EN_Td; avg_en]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FIRST_TOWNSEND_COEFF.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','alpha/N (m^2)');
            fprintf(fid,fmt,[EN_Td; alpha_N]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_MOBILITY.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','mu*N (m V s)^(-1)');
            fprintf(fid,fmt,[EN_Td; muN_f0]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/DIFFUSION_COEFF.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D*N (m s)^(-1)');
            fprintf(fid,fmt,[EN_Td; DN_f0]);
            fclose(fid);
            
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/NON_CONSERVATIVE_DIFFUSION_COEFF.txt'],'a+');
            fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D*N (m s)^(-1)');
            fprintf(fid,fmt,[EN_Td; DN_f0_nu]);
            fclose(fid);         

            if GE == 1
                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_MOBILITY.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','mu_BULK*N (m V s)^(-1)');
                fprintf(fid,fmt,[EN_Td; w_BULK/E0*Np]);
                fclose(fid);
                
                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_DRIFT_VELOCITY.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','W_BULK (m/s)');
                fprintf(fid,fmt,[EN_Td; w_BULK]);
                fclose(fid);

                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_TRANSVERSE_DIFFUSION_COEFF.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D_T*N (m s)^(-1)');
                fprintf(fid,fmt,[EN_Td; DT_BULK*Np]);
                fclose(fid);

                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_LONGITUDINAL_DIFFUSION_COEFF.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D_L*N (m s)^(-1)');
                fprintf(fid,fmt,[EN_Td; DL_BULK*Np]);
                fclose(fid);    
                
                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_TRANSVERSE_DIFFUSION_COEFF.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D_T*N (m s)^(-1) (FLUX ONLY)');
                fprintf(fid,fmt,[EN_Td; DFT_N]);
                fclose(fid);

                fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_LONGITUDINAL_DIFFUSION_COEFF.txt'],'a+');
                fprintf(fid,'%12s \t %12s \t \n','E/N (Td)','D_L*N (m s)^(-1) (FLUX ONLY)');
                fprintf(fid,fmt,[EN_Td; DFL_N]);
                fclose(fid); 
            end
        end
    end
else    
    dlmwrite(['Exported_Data/Run_',num2str(RUN_ID),'/EEDFs/',num2str(EN_Td),'_Td.txt'],[ue/qe; f_0]','delimiter','\t','precision','%5e');
    
    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/CONVERGENCE_FLAGS.txt'],'a+');
    if GE == 1
        fprintf(fid,'%5e \r %6.1f \r %6.1f \r %6.1f \r %6.1f \r \n',[EN_Td; converged_f(is); converged_f1L(is); converged_f2T(is); converged_f2L(is)]);
    else
        fprintf(fid,'%5e \r %6.1f \r \n',[EN_Td; converged_f(is)]);
    end
    fclose(fid);
    
    for Ng = 1:N_gases
        
        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/ELASTIC_RATE.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; k_ela(Ng)]);
        fclose(fid);
        
        for i_att = 1:N_atts(Ng)
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/ATTACHMENT_RATE_',num2str(i_att),'.txt'],'a+');
            fprintf(fid,fmt,[EN_Td; k_att(i_att,Ng)]);
            fclose(fid);
        end     
        for i_exc = 1:N_excs(Ng)
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/EXCITATION_RATE_',num2str(i_exc),'.txt'],'a+');
            fprintf(fid,fmt,[EN_Td; k_exc(i_exc,Ng)]);
            fclose(fid);
        end
        for i_iz = 1:N_izs(Ng)
            fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Gas_',num2str(Ng),'/IONIZATION_RATE_',num2str(i_iz),'.txt'],'a+');
            fprintf(fid,fmt,[EN_Td; k_iz(i_iz,Ng)]);
            fclose(fid);
        end                 
    end

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/ELASTIC_RATE.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; sum(k_ela)]);
    fclose(fid);            

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/ATTACHMENT_RATE.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; k_att_total]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/EXCITATION_RATE.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; sum(sum(k_exc))]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/Total/IONIZATION_RATE.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; k_iz_total]);
    fclose(fid);
    
    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/IONIZATION_RATE.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; k_iz_total]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/MEAN_ENERGY.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; avg_en]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FIRST_TOWNSEND_COEFF.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; alpha_N]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_MOBILITY.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; muN_f0]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/DIFFUSION_COEFF.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; DN_f0]);
    fclose(fid);

    fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/NON_CONSERVATIVE_DIFFUSION_COEFF.txt'],'a+');
    fprintf(fid,fmt,[EN_Td; DN_f0_nu]);
    fclose(fid);

    if GE == 1
        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_MOBILITY.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; w_BULK/E0*Np]);
        fclose(fid);
        
        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_DRIFT_VELOCITY.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; w_BULK]);
        fclose(fid);
        
        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_TRANSVERSE_DIFFUSION_COEFF.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; DT_BULK*Np]);
        fclose(fid);

        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/BULK_LONGITUDINAL_DIFFUSION_COEFF.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; DL_BULK*Np]);
        fclose(fid);    

        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_TRANSVERSE_DIFFUSION_COEFF.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; DFT_N]);
        fclose(fid);

        fid = fopen(['Exported_Data/Run_',num2str(RUN_ID),'/FLUX_LONGITUDINAL_DIFFUSION_COEFF.txt'],'a+');
        fprintf(fid,fmt,[EN_Td; DFL_N]);
        fclose(fid);         
    end
end