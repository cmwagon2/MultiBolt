// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_SPECIESERASE
#define COLLISIONLIBRARY_SPECIESERASE
#include "collisionlibrary"

std::vector<std::shared_ptr<lib::AbstractXsec>>::iterator
	lib::Species::erase_safely_from_all(std::vector<std::shared_ptr<lib::AbstractXsec>>::iterator it) {

	lib::CollisionCode c = (*it)->code();

	XsecPtrVec* ptr = pick_from_code(c);

	// erase all xsecs in *ptr which are the same process as that deleted from all 
	for (auto it2 = (*ptr).begin(); it2 != (*ptr).end(); it2++) {

		if ((*it2)->process().compare((*it)->process()) == 0) {

			lib::debug_statement("Erased Xsec with process [" + (*it2)->process() + "]");
			it2 = (*ptr).erase(it2);
		}

		if (it2 == (*ptr).end()) { break; };
	}

	it = allcollisions.erase(it);
	return it;
}



void lib::Species::erase_by_process(std::string process) {
	for (auto it = allcollisions.begin(); it != allcollisions.end();) {
		if ((*it)->process().compare(process) == 0) {

			it = erase_safely_from_all(it);

			if (it == allcollisions.end()) { break; };
		}
		else {
			it++;
		}
	}
}

// to erase nth collision regardless of type
void lib::Species::erase_by_index(int k) {

	if (k > n_all()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = allcollisions.at(k)->process();
	erase_by_process(temp_process);
}

// case for when the collision-type is known
void lib::Species::erase_by_index(lib::CollisionCode c, int k) {

	XsecPtrVec* ptr = pick_from_code(c);

	if (k > ptr->size()) {
		lib::bad_index(k);
		return;
	}

	std::string temp_process = (*ptr).at(k)->process();
	erase_by_process(temp_process);
}

#endif