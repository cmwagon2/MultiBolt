// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LIBRARYERASE
#define COLLISIONLIBRARY_LIBRARYERASE
#include "collisionlibrary"


void lib::Library::erase_by_name(std::string name) {



	for (auto it = allspecies.begin(); it != allspecies.end();) {

		if ((*it)->_name.compare(name) == 0) {

			lib::debug_statement("Species with name [" + (*it)->_name + "] removed from library.");

			it = allspecies.erase(it);
			

		}
		else {
			++it;

		}

		if (it == allspecies.end()) {
			break;
		}

	}

}

// to erase nth species regardless of type
void lib::Library::erase_by_index(int k) {

	if (k > n_all()) {
		lib::bad_index(k);
		
	}

	std::string temp_process = allspecies.at(k)->_name;
	erase_by_name(temp_process);
}


void lib::Library::keep_only(const std::vector<std::string>& names) {

	for (auto& name : names) {
		if (!has_species(name)) {
			lib::species_not_found(name);
		}
	}

	std::set<std::string> temp;
	for (auto name : names) {
		temp.insert(name);
	}


		for (auto it = allspecies.begin(); it != allspecies.end();) {


			// is the count of this name zero in the set of asked-for names
			if (temp.count((*it)->_name) == 0){

				lib::debug_statement("Species with name [" + (*it)->_name + "] removed from library.");
				it = allspecies.erase(it);
				
			}
			else {
				++it;

			}

			if (it == allspecies.end()) {
				break;
			}

		}

	return;
}

void lib::Library::keep_only(const std::string name) {
	keep_only(std::vector<std::string>({ name }));
}

// Erase species from the library that have frac == 0
void lib::Library::remove_all_zero_frac() {
	for (auto it = allspecies.begin(); it != allspecies.end();) {


		// is the count of this name zero in the set of asked-for names
		if ((*it)->_frac == 0.0) {

			lib::debug_statement("Species with name [" + (*it)->_name + "] removed from library.");
			it = allspecies.erase(it);

		}
		else {
			++it;

		}

		//if (it == allspecies.end()) {
		//	break;
		//}
	}
}

#endif
