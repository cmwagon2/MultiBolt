// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2023 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once
#ifndef COLLISIONLIBRARY_LIBRARY
#define COLLISIONLIBRARY_LIBRARY
#include "collisionlibrary"


// A container of many species. Is intended to hold no duplicate species.
// Member functions allow definition of adding species by files
// and by adding single species
class Library {


public:



	typedef std::vector< std::shared_ptr<lib::Species>> SpecPtrVec;

	Library() {}

	Library(const std::vector<std::string> Xsec_fids) {
		add_xsecs_from_LXCat_files(Xsec_fids);
	}

	Library(const std::string Xsec_fid) {
		add_xsecs_from_LXCat_files(Xsec_fid);
	}



	SpecPtrVec allspecies;

	auto n_all() { return allspecies.size(); }
	

	void assign_fracs_by_names(std::vector<std::pair<std::string, double>> pairs);
	void assign_fracs_by_names(std::vector<std::string> names, std::vector<double> fracs);

	void assign_fracs_by_names(std::string name, double frac);

	void assign_frac_by_index(int k, double frac);


	bool has_species(const std::string& name);


	void add_Xsec(std::shared_ptr<lib::AbstractXsec> x);
	void add_Xsec(lib::FunctionalXsec x);
	//void add_Xsec(lib::DiscreteXsec x);

	void add_Xsecs_from_library(lib::Library this_lib); // 11/17/2022

	void add_blank_species(const std::string& name);
	void add_blank_species(const std::vector<std::string>& names);

	void add_xsecs_from_LXCat_files(const std::vector<std::string>& Xsec_fids);
	void add_xsecs_from_LXCat_files(const std::string& Xsec_fid);
	
	
	void erase_by_name(std::string name);
	void erase_by_index(int k);
	

	void keep_only(const std::vector<std::string>& names);
	void keep_only(const std::string name);

	void remove_all_zero_frac();

	void assign_scattering_by_type(lib::Scattering scattering, lib::CollisionCode c);
	void assign_scattering_by_type(lib::ScatteringCode s, lib::CollisionCode c, double screen_eV);




	void print() {

		std::stringstream ss; ss.str("");
		ss.setf(std::ios_base::scientific, std::ios_base::floatfield);

		ss << "\n";
		ss << "Library: " << "\n";
		

		for (auto spec : allspecies) {
			ss << spec->_name << "\t" << spec->_frac << "\n";

			for (auto& x : spec->allcollisions) {
				ss << "\t" << "x" + std::to_string(x->scale()) << "\t" << x->process() << "\t" << x->scattering()->name << "\n";
			}
		}

		std::cout << ss.str() << std::endl;


	};

	bool is_valid() {

		std::set<std::string> names;

		// do duplicate species exist?
		for (auto spec : allspecies) {
			names.insert(spec->name());
		}

		if (names.size() != n_all()) {
			
			lib::normal_statement("Library is invalid because duplicate species exist.");
			return false;
		}


		// are any of the inside species invalid on their own?
		for (auto spec : allspecies) {
			if (spec->is_valid()) {
				lib::normal_statement("Library is invalid: species " + spec->name() + " was found to be invalid.");
				return false;
			}
		}


		return true;
	}



	void clear() {
		allspecies = {};
	}


	std::shared_ptr<lib::Species> get_species(const std::string& name);

	void add_species_from_LXCat_file(const std::string& Xsec_fid);


};


bool lib::Library::has_species(const std::string& name) {


	for (auto spec : allspecies) {

		if (lib::same_string(spec->_name, name)) {
			return true;
		}

	}

	return false;

}

std::shared_ptr<lib::Species> lib::Library::get_species(const std::string& name) {

	if (!has_species(name)) {

		lib::species_not_found(name);

		return nullptr;
	}


	for (auto it = allspecies.begin(); it != allspecies.end(); it++) {

		if (lib::same_string(name, (*it)->_name)) {
			return (*it);
		}
	}

	return nullptr;
}




#endif