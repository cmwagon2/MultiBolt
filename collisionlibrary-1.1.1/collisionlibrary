// SPDX-License-Identifier: MIT
//
// CollisionLibrary - management of electron collisional cross sections for low temperature plasma studies
// 
// Copyright 2021-2022 Max Flynn
// 
// Licensed under the MIT License.
// A copy of the license should be distributed with this software (see LICENSE).
// The license text may also be found at: https://opensource.org/licenses/MIT
// --------------------------------------------------------------------------------- 

#pragma once

#ifndef COLLISIONLIBRARY_INCLUDE
#define COLLISIONLIBRARY_INCLUDE


// reminder: use C++17 language standard
// problems come up with filesystem otherwise

// 12/2022 - have to do the same lapack-exclusion to avoid linking errors in case coll-lib is called on its own
#if !defined(ARMA_DONT_USE_LAPACK)
    #define ARMA_DONT_USE_LAPACK
#endif  

#include <armadillo>
#include <iostream>
#include <cstdarg>
#include <string>
#include <fstream>
#include <memory>
#include <cstdio>

#include <stdexcept>
#include <stdio.h>
#include <string>
#include <vector>
#include <memory>
#include <filesystem>
#include <set>




#include <iomanip>

namespace lib{

    #include "collisionlibrary_src/const.hpp"
	#include "collisionlibrary_src/enum.hpp"
	#include "collisionlibrary_src/util.hpp"
	

	#include "collisionlibrary_src/Speaker/Speaker.hpp"
	#include "collisionlibrary_src/Speaker/statement.hpp"
	#include "collisionlibrary_src/Speaker/error.hpp"
	
	#include "collisionlibrary_src/Scattering/Scattering.hpp"

	#include "collisionlibrary_src/Info/Info.hpp"
	#include "collisionlibrary_src/Info/parse_LXCat_info_from_raw_header.hpp"
	

	
	#include "collisionlibrary_src/Xsec/Xsec_interpolation.hpp"

	#include "collisionlibrary_src/Xsec/AbstractXsec.hpp"
	#include "collisionlibrary_src/Xsec/DiscreteXsec.hpp"
	#include "collisionlibrary_src/Xsec/FunctionalXsec.hpp"

	#include "collisionlibrary_src/Xsec/make_Xsecs_from_LXCat_file.hpp"


	#include "collisionlibrary_src/Species/Species.hpp"
	#include "collisionlibrary_src/Species/Species_add.hpp"
	#include "collisionlibrary_src/Species/Species_erase.hpp"
	#include "collisionlibrary_src/Species/Species_assign.hpp"

	#include "collisionlibrary_src/Library/identify_species_in_LXCat_file.hpp"
	#include "collisionlibrary_src/Library/Library.hpp"
	#include "collisionlibrary_src/Library/Library_add.hpp"
	#include "collisionlibrary_src/Library/Library_erase.hpp"
	#include "collisionlibrary_src/Library/Library_assign.hpp"
	
	#include "collisionlibrary_src/AnalyticXsecs/ReidRamp.hpp"
	#include "collisionlibrary_src/AnalyticXsecs/LucasSalee.hpp"
	#include "collisionlibrary_src/AnalyticXsecs/BornApprox_rotation.hpp"

}

#endif
