import configparser
import const as c 

from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QPushButton, QTableWidgetItem, QWidget, QListWidget, QMainWindow, QHBoxLayout, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QMessageBox, QFileDialog, QVBoxLayout, QTableWidget, QCheckBox, QTextEdit, QTreeView, QFileSystemModel
from PyQt5.QtWidgets import QAbstractItemView
#from PyQt5.QtGui import QAbstractItemView

from PyQt5.QtCore import QModelIndex, pyqtSlot

import shutil
import os



class ViewDataWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()
        self.set_defaults()
    
    def home(self):
        self.model = QFileSystemModel(self)
        self.model.setRootPath(c.HOME)

        self.viewingTree = QTreeView(self)
        self.viewingTree.setModel(self.model)
        self.viewingTree.setRootIndex(self.model.index(c.HOME))
        self.viewingTree.setColumnHidden(1, True)
        self.viewingTree.setColumnHidden(2, True)
        self.viewingTree.setColumnHidden(3, True)

        self.viewingTree.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.txtViewer = QTextEdit(self)
        self.txtViewer.setReadOnly(True)


        
        self.changeExportView = QPushButton("Change View")
        
        self.currentExportView = QLineEdit(self)
        self.currentExportView.setReadOnly(True)
        
        #self.setSizes(self.WINDOW_WIDTH, self.WINDOW_HEIGHT)

        # ----
        self.clearAllExport = QPushButton("Erase All Runs")
        self.clearAllExport.setStyleSheet("background-color: " + c.RED_COLOR)

        self.clearOneExport = QPushButton("Erase Selected Runs")

        
        pass

    def connects(self):
        self.viewingTree.selectionModel().selectionChanged.connect(self.handletxtViewer)
        self.changeExportView.clicked.connect(self.handleChangeDir)
        self.currentExportView.textChanged.connect(lambda: self.model.setRootPath(self.currentExportView.text()))

        self.clearAllExport.clicked.connect(self.clearAllExportFunc)
        self.clearOneExport.clicked.connect(self.clearOneExportFunc)

        pass

    def layout(self):
        layout = QVBoxLayout()

        topHbox = QHBoxLayout()
        topHbox.addWidget(self.changeExportView)
        topHbox.addWidget(self.currentExportView)

        topHbox.addWidget(self.clearOneExport)
        topHbox.addWidget(self.clearAllExport)
        
        bottomHbox = QHBoxLayout()
        bottomHbox.addWidget(self.viewingTree)
        bottomHbox.addWidget(self.txtViewer)

        layout.addLayout(topHbox)
        layout.addLayout(bottomHbox)
        self.setLayout(layout)

        pass


    def handleChangeDir(self):
        filename = QFileDialog.getExistingDirectory(self, 'Open Dir', c.HOME)
        if filename == "":
            return

        self.currentExportView.setText(filename)

        self.model.setRootPath(filename)
        self.viewingTree.setModel(self.model)
        self.viewingTree.setRootIndex(self.model.index(filename))



    def handletxtViewer(self):
        self.txtViewer.clear()

        try:
            index = self.viewingTree.selectionModel().selection().indexes()[0]
        except:
            return

        path = self.model.filePath(index)

        #print(os.path.splitext(path)[1])

        if os.path.splitext(path)[1] == '.txt':
            self.txtViewer.clear()
            with open(path,'r') as fh:
                all_lines = fh.readlines()

            for line in all_lines:
                self.txtViewer.insertPlainText(line)

        return

    def check_is_run(self, dirname):
        if os.path.exists(os.path.join(dirname, 'RUN_DETAILS.txt')):
            return(True)
        else:
            return(False)
        pass

    def clearAllExportFunc(self):

        # prompt first with a warning text box
        qm = QMessageBox()
        ret = qm.question(self, '', "Are you sure you want to erase all runs?", qm.Yes | qm.No)

        if ret == qm.No:
            return



        self.viewingTree.collapseAll()

        dirindex = self.viewingTree.rootIndex()
        dirpath = self.model.filePath(dirindex)
        dirlist = next(os.walk(dirpath))[1]

        self.viewingTree.selectAll()

        self.clearOneExportFunc()

        indexes = self.viewingTree.selectionModel().selectedIndexes()
        #for i in indexes:
        self.viewingTree.clearSelection()

        #self.viewingTree.deselectAll()

        pass

    def clearOneExportFunc(self):

        indexes = self.viewingTree.selectionModel().selectedIndexes()

        for i in indexes:
            temp = self.model.filePath(i)

        # not sure why duplicate indexes exist. call them duplicates if they have the same filePath
        ix_to_delete = []
        unique_names = []
        for ix in range(0, len(indexes)):
            if not (self.model.filePath(indexes[ix]) in unique_names):
                unique_names.append(self.model.filePath(indexes[ix]))
            else:
                ix_to_delete.append(ix)

        for index in sorted(ix_to_delete, reverse=True):
            del indexes[index]


        for i in indexes:
            dirname = self.model.filePath(i)
            if self.check_is_run(dirname):
                self.remove_tree_index(i)
            else:
                print("GUI: Will not remove directory ", dirname, ". Does not contain RUN_DETAILS.txt, is not a MB run.")

        pass

    def remove_tree_index(self, index):
        name = self.model.filePath(index)
        try:
            shutil.rmtree(name)
        except PermissionError:
            print("GUI: Cannot remove folder ", name, ": Permission Denied")
        except:
            pass

    def set_defaults(self):

        try:

            config = configparser.ConfigParser()
            config.read(c.ini_name)

            default = config['Default']

            self.model.setRootPath(c.HOME)
            path = (c.EXPORT_PATH).replace("\\", "/")
            self.viewingTree.setRootIndex(self.model.index(path))

            self.currentExportView.setText(path)

        except:
            return
      


if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = ViewDataWidget(window)

    x.show()
 
    sys.exit(app.exec_())