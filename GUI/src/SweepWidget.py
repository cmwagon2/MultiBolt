import configparser
import os

from PyQt5.QtWidgets import QApplication, QGroupBox, QPushButton, QWidget, QMainWindow, QCheckBox, QLabel, QLineEdit
from PyQt5.QtWidgets import QTextEdit, QComboBox, QVBoxLayout, QCheckBox, QFormLayout, QTabWidget, QRadioButton


variables_dir = {"Field Strength" : 0,
                "Grid Points" : 1,
                "Terms" : 2,
                "Temperature" : 3,
                "Pressure" : 4}

alt_variables_dir = {"EN_Td" : 0,
                "Nu" : 1,
                "N_terms" : 2,
                "T_K" : 3,
                "p_Torr" : 4}

sweeps_dir = {"Single" : 0,
            "LinSpace" : 1,
            "LogSpace" : 2,
            "RegSpace" : 3}




class SweepWidget(QWidget):
    def __init__(self, parent=None):
        super(QWidget, self).__init__()
        self.home()
        self.connects()
        self.layout()

        self.points.setMaximumHeight(self.lin_points.height() * 2)


    def home(self):
        self.variable_choice = QComboBox(self)
        self.variable_choice.addItems(variables_dir.keys())
        #self.variable_choice.setMaximumWidth(QLineEdit().sizeHint().width())

        self.tabs = QTabWidget(self)
        #self.tabs.setMaximumWidth(QPushButton("X").sizeHint().width())

        

        self.lin_start = QLineEdit(self)
        self.lin_stop = QLineEdit(self)
        self.lin_points = QLineEdit(self)

        self.log_start = QLineEdit(self)
        self.log_stop = QLineEdit(self)
        self.log_points = QLineEdit(self)

        self.reg_start = QLineEdit(self)
        self.reg_step = QLineEdit(self)
        self.reg_stop = QLineEdit(self)

        self.points = QTextEdit(self)

        self.home_points()
        self.home_lin()
        self.home_log()
        self.home_reg()

        self.set_defaults()
        


    def connects(self):
        pass
    
    def layout(self):
        layout = QVBoxLayout()

        form = QFormLayout()
        form.addRow("Variable", self.variable_choice)
        form.addRow(self.tabs)

        group = QGroupBox("Sweep Settings")
        group.setLayout(form)
        layout.addWidget(group)

        self.setLayout(layout)

       
        pass

    def home_points(self):
        clear = QPushButton("Clear")
        clear.clicked.connect(self.points.clear)

        w = QWidget()
        layout = QVBoxLayout(w)
        layout.addWidget(QLabel("Input one (or more) comma-seperated values."))
        layout.addWidget(self.points)
        layout.addWidget(clear)
        w.setLayout(layout)
        self.tabs.addTab(w, "Points")


    def home_lin(self):
        w = QWidget()
        layout = QFormLayout(w)
        layout.addRow("Start", self.lin_start)
        layout.addRow("Stop", self.lin_stop)
        layout.addRow("N", self.lin_points)
        w.setLayout(layout)
        self.tabs.addTab(w, "Linear")

    def home_log(self):
        w = QWidget()
        layout = QFormLayout(w)
        layout.addRow("Start", self.log_start)
        layout.addRow("Stop", self.log_stop)
        layout.addRow("N", self.log_points)
        w.setLayout(layout)
        self.tabs.addTab(w, "Log10")

    def home_reg(self):
        w = QWidget()
        layout = QFormLayout(w)
        layout.addRow("Start", self.reg_start)
        layout.addRow("Stop", self.reg_stop)
        layout.addRow("Delta", self.reg_step)
        
        w.setLayout(layout)
        self.tabs.addTab(w, "Regular")

    def get_var_option(self):
        return self.variable_choice.currentText()

    def get_sweep_option(self):
        return self.tabs.tabText(self.tabs.currentIndex())

    def get_args(self):
        swp = self.get_sweep_option()
        args = ""

        args = args + "--sweep_option "
        var = self.get_var_option()


        if var == "Field Strength":
            args = args + "EN_Td "
        elif var == "Grid Points":
            args = args + "Nu "
        elif var == "Terms":
            args = args + "N_terms "
        elif var == "Temperature":
            args = args + "T_K "
        elif var == "Pressure":
            args = args + "p_Torr "


        args = args + "--sweep_style "

        if swp == "Points":
            txt = self.points.toPlainText().replace(",", " ")
            splitted = txt.strip().split(" ")
            args = args + "def "

            if args == "Grid Points" or args == "Terms":
                for s in splitted:
                    if len(s) > 0:
                        try: int(s); args = args + " {}".format(s)
                        except: args + args + "ERROR-(int(value)) "
            else:

                for s in splitted:
                    if len(s) > 0:
                        try: float(s); args = args + " {}".format(s)
                        except: args + args + "ERROR-(float(value)) "


        elif swp == "Linear":
            args = args + "lin "

            try: float(self.lin_start.text()); args = args + "{} ".format(self.lin_start.text())
            except: args = args + "ERROR-(float(start)) "

            try: float(self.lin_stop.text()); args = args + "{} ".format(self.lin_stop.text())
            except: args = args + "ERROR-(float(stop)) "

            try: int(self.lin_points.text()); args = args + "{} ".format(self.lin_points.text())
            except: args = args + "ERROR-(int(points)) "

        elif swp == "Log10":
            args = args + "log "

            try: float(self.log_start.text()); args = args + "{} ".format(self.log_start.text())
            except: args = args + "ERROR-(float(start)) "

            try: float(self.log_stop.text()); args = args + "{} ".format(self.log_stop.text())
            except: args = args + "ERROR-(float(stop)) "

            try: int(self.log_points.text()); args = args + "{} ".format(self.log_points.text())
            except: args = args + "ERROR-(int(points)) "

        elif swp == "Regular":
            args = args + "reg "

            try: float(self.reg_start.text()); args = args + "{} ".format(self.reg_start.text())
            except: args = args + "ERROR-(float(start)) "

            try: float(self.reg_stop.text()); args = args + "{} ".format(self.reg_stop.text())
            except: args = args + "ERROR-(float(stop)) "

            try: float(self.reg_step.text()); args = args + "{} ".format(self.reg_step.text())
            except: args = args + "ERROR-(float(delta)) "

            

        return args

    def set_defaults(self):
        
        try:
            config = configparser.ConfigParser()
            config.read("config.ini")
            default = config['Default']

            self.variable_choice.setCurrentIndex(alt_variables_dir[default["sweep_option"]])

            args = default["sweep_style"].split()



            self.clear_sweepoptions()

            if args[0] == "def":
                N = len(args)
                line = "{}".format(args[1])
                for i in range(2, N, 1):
                    line = line + ", {}".format(args[i])
                self.points.setText(line)
                self.tabs.setCurrentIndex(0)

            elif args[0] == "lin":
                self.lin_start.setText(args[1])
                self.lin_stop.setText(args[2])
                self.lin_points.setText(args[3])
                self.tabs.setCurrentIndex(1)

            elif args[0] == "log":
                self.log_start.setText(args[1])
                self.log_stop.setText(args[2])
                self.log_points.setText(args[3])
                self.tabs.setCurrentIndex(2)

            elif args[0] == "reg":
                self.reg_start.setText(args[1])
                self.reg_stop.setText(args[2])
                self.reg_step.setText(args[3])
                
                self.tabs.setCurrentIndex(3)

        except:
            return

        return args

    def clear_sweepoptions(self):
        self.points.clear()

        self.lin_start.clear()
        self.lin_stop.clear()
        self.lin_points.clear()

        self.log_start.clear()
        self.log_stop.clear()
        self.log_points.clear()

        self.reg_start.clear()
        self.reg_stop.clear()
        self.reg_step.clear()



if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = SweepWidget(window)

    x.show()

    print(x.get_args())

    sys.exit(app.exec_())