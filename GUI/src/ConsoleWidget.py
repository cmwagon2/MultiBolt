import sys


from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject
from PyQt5.QtWidgets import QApplication, QPushButton, QWidget, QMainWindow
from PyQt5.QtWidgets import QLabel, QVBoxLayout, QTextEdit

from PyQt5.QtGui import QTextCursor


class ConsoleWidget(QWidget):

    emitConsole = pyqtSignal(str)

    def __init__(self, parent=None):
            super(QWidget, self).__init__()
            self.home()
            self.connects()
            self.layout()
            

    def home(self):
        self.emitConsole.connect(self.write)
        self.clearButton = QPushButton("Clear console", self)
        self.txt = QTextEdit(self)
        self.txt.setReadOnly(True)

        self.txt.setMinimumWidth(QLabel().width() * 1.2)

    def connects(self):
        self.clearButton.clicked.connect(self.txt.clear)

    def layout(self):
        layout = QVBoxLayout(self)
        layout.addWidget(self.clearButton)
        layout.addWidget(self.txt)
        self.setLayout(layout)

    def write(self, mystr):
        self.txt.append(mystr.strip())
        self.txt.ensureCursorVisible()
        self.txt.moveCursor(QTextCursor.End)
        self.txt.verticalScrollBar().setValue(self.txt.verticalScrollBar().maximum())



# Lets the python outstream be printed to a Qtexteditor
class Stream(QObject):
    newText = pyqtSignal(str)
    def write(self, text):
        self.newText.emit(str(text))


if __name__ == "__main__":

    import sys

    app = QApplication(sys.argv)
    window = QMainWindow()
    x = ConsoleWidget(window)

    x.show()
 
    sys.exit(app.exec_())