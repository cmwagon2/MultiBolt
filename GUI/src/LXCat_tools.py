# Max Flynn
# 2020 - 2022
# Necessary for GUI - tell the user what species and processes can be found in
# LXCat formatted file. Ie, to match what MultiBolt could tell just by exe.


all_colls = ["ATTACHMENT",
             "ELASTIC",
             "EFFECTIVE",
             "EXCITATION",
             "IONIZATION",
             "ROTATIONAL",
             "VIBRATIONAL"]

invertible_colls = [all_colls.index("EXCITATION"),
                    all_colls.index("ROTATIONAL"),
                    all_colls.index("VIBRATIONAL")]

odd_format_colls = [all_colls.index("ROTATIONAL"),
                    all_colls.index("VIBRATIONAL")]




singleArrow = "->"
doubleArrow = "<->"


# ---
def reactant_from_process(proc):
    ls = []
    if doubleArrow in proc:
        ls = proc.split(doubleArrow)
    elif singleArrow in proc:
        ls = proc.split(singleArrow)

    return ls[0].strip("E + ")


# ---
def product_from_process(proc):
    ls = []
    if doubleArrow in proc:
        ls = proc.split(doubleArrow)
    elif singleArrow in proc:
        ls = proc.split(singleArrow)

    for key in all_colls:
        if key in ls[-1].upper():
            newls = ls[-1].split(", {}".format(key.capitalize()))
            return newls[0].strip("E +").strip()


# ---
def remove_duplicate_species(specs_list):
    return list(dict.fromkeys(specs_list))


# ---
def find_species_in_LXCat(filename):
    specs_list = list()

    # case to read from multiple files in a row
    if isinstance(filename, list):
        for file in filename:
            specs_in_file = find_species_in_LXCat(str(file))
            specs_list.extend(specs_in_file)

        return remove_duplicate_species(specs_list)

    try:
        read = open(filename, 'r')
        read.close()

    except FileNotFoundError:
        print("Xsec file {} could not be opened.".format(filename))
        return []

    # begin actually reading
    with open(filename, 'r') as f:

        for index, line in enumerate(f):
            for key in all_colls:

                if key == line.strip():

                    specs = []
                    nextline = f.readline()

                    # contingency for unusual format cases
                    if all_colls.index(key) in odd_format_colls:
                        nextline = f.readline()
                        proc = nextline.strip("PROCESS: ")

                        reactant = reactant_from_process(proc)
                        product = product_from_process(proc)
                        specs_list.append(reactant)
                        specs_list.append(product)
                        break

                    else:


                        # this tends to happen with elastics or single-species
                        if not (singleArrow in nextline) and not (doubleArrow in nextline):
                            specs_list.append(nextline.strip())
                            break

                        # for multi-species
                        if doubleArrow in nextline:
                            specs = nextline.split(doubleArrow)
                        elif singleArrow in nextline:
                            specs = nextline.split(singleArrow)

                        for i, s in enumerate(specs, start=0):
                            # print("Trying to add species: {}".format(s.strip()))
                            specs_list.append(specs[i].strip())

                        # print("Found species: {}".format(cleanspecs))

                        break

    # remove duplicate entries - yes this is faster as a set, but I'd prefer to preserve the order
    return remove_duplicate_species(list(dict.fromkeys(specs_list)))


##---
def find_processes_in_LXCat(filename):
    proc_list = list()
    if isinstance(filename, list):
        # print("was found to be list!")
        for f in filename:
            # print("f is: {}".format(f))
            proc_list.extend(find_processes_in_LXCat(f))

        return proc_list

    try:
        read = open(filename, 'r')
        read.close()
    except FileNotFoundError:
        print("The Xsec file {} could not be opened".format(filename))
        return []

    with open(filename, 'r') as reader:

        for i, line in enumerate(reader):
            if "PROCESS:" in line:
                # print(line)
                proc_list.append(line.strip("PROCESS:").strip())

    # for every known excitation, also pass in the presumed existing superelastic
    sup_list = []

    simple_proc_list = []
    for proc in proc_list :
        proc = proc.upper()

        for coll in all_colls:
            if coll in proc:
                simple_proc_list.append(all_colls.index(coll))
                break

    for proc in proc_list:
        index = proc_list.index(proc)

        # ("Excitation" or "Rotational")
        if simple_proc_list[index] in invertible_colls:
            reactant = reactant_from_process(proc)
            product = product_from_process(proc)

            sup_list.append("E + {} -> E + {}, Superelastic (derived)".format(product, reactant))

    proc_list.extend(sup_list)

    return remove_duplicate_species(list(dict.fromkeys(proc_list)))


# -- trial script
if __name__ == "__main__":
    # Xsec_fids = ["../../cross-sections/Biagi_N2.txt", "../../cross-sections/Biagi_SF6.txt"]
    #Xsec_fids = ["../../cross-sections/Biagi_N2.txt"]

    #Xsec_fids = ["../../applications/CO_rot/CO_rot_cross-sections/ISTLisbon/ISTLisbon_CO-rot.txt"]

    Xsec_fids = ["C:/Users/Max/Documents/Work/Projects/Sandia_alt_SF6/Explorations/ResultsXsecStitcher/C4F7N_RECENT_XSEC.txt"]

    print("Running as script, debug mode by reading test file: {}".format(Xsec_fids))
    print("Found the following unique species:")
    specs = find_species_in_LXCat(Xsec_fids)
    for s in specs:
        print(s)

    print("Found the following unique processes:")
    procs = find_processes_in_LXCat(Xsec_fids)
    for p in procs:
        print(p)

    print("Leaving test script")
