Notes, 1/2022

Armadillo is a linear-algebra library used for sparse-matrix implementation and for its API for SuperLU.

For the purposes of MultiBolt, it is a header-only library.
Look inside the armadillo source directory for "armadillo_src/include/"
You are, specifically, seeking to include the same directory as which contains the extensionless file 'armadillo': this is an includes-file.

If your system path *knows* that this location of armadillo exists, you may also just call #include <armadillo> at the top of your source

For future safe-keeping, I have included a zipped archive of armadillo known to work with MultiBolt and SuperLU.
If you are without a good armadillo repository, unzip this archive and use as-is.

-Max Flynn