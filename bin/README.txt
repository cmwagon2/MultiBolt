These binaries are provided to streamline the use of MultiBolt.
As is the case with all pre-compiled binaries, USE AT YOUR OWN RISK.

-----

These are all compilations of the application script "command.cpp", which is a terminal-argument driven version of MultiBolt.
Executing without arguments will trigger a verbose default run.
Execute with the argument "--help" for argument instrctions (also provided in /docs).

'multibolt_win64.exe' was compiled on a Windows 10 x64-bit machine using MSVC.
(Windows users: if you get a sort of "missing .dll" error, this is an easy fix: you are missing a free Microsoft visual-C redistributable)

'multibolt_linux' was compiled specifically on OpenSUSE 15.2, but should work for Linux machines in general.

-----

To take full advantage of your particular machine, or to solve in complicated conditions, you are highly encouraged to use multibolt as a header-library and try compiling your own binaries. CMake (for Linux+Windows) and VisualStudio (for Windows) are currently suported as out-of-the-box build methods.

-Max Flynn, 03/25/2022
