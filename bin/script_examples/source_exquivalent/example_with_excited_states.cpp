// MultiBolt v3.0.0
// Example script: read LXCat cross section files for collisions and
// and set source to run a single calculation, including a chosen excited state,
// then export the data.

#include "multibolt"
#include <vector>


int main() {

	// stdout mode
	mb::mbSpeaker.printmode_normal_statements();
	// mb::mbSpeaker.printmode_debug_statements();
	// mb::mbSpeaker.printmode_silent();


	/* Collision Library Definition */

	// The Xsec files that have the collisions we are interested in
	std::vector<std::string> Xsec_fids = { "../../../../../../cross-sections/Biagi_N2.txt" };

	// Say we are interested in all collisions of the neutral, as well as the first fibrational excited state
	// (its super elastic collision specifically)
	std::vector<std::string> names = { "N2", "N2( VIB V1)" };  // name must be exact!

	// Prevalence of the two species. Excitation degree is normally small, so assign
	// the excited state as a small amount.
	std::vector<double> fracs = { (1 - 1e-5), 1e-5 };

	// Begin with an empty library
	lib::Library lib = lib::Library();

	lib.add_xsecs_from_LXCat_files(Xsec_fids);	// add all collisions from all files
	lib.keep_only(names);				// erase all collisions which do not belong to the species we are interested in
	lib.assign_fracs_by_names(names, fracs);	// declare the fraction of the species


	/* BoltzmannParameters Definition */

	mb::BoltzmannParameters p = mb::BoltzmannParameters();

	p.model = mb::ModelCode::HDGE;
	p.N_terms = 6;

	p.Nu = 200;

	p.iter_max = 100;	// Force break if you haven't converged within this many iterations to avoid wasting time
	p.iter_min = 4;		// Force continue if below this number of iterations
	p.weight_f0 = 1.0;	// Iteration weight. N2 and Ar behave well, 1.0 will suffice.

	p.conv_err = 1e-6;	// relative error


	p.USE_ENERGY_REMAP = true;	// Look for a new grid if necessary
	p.remap_grid_trial_max = 10;				// try a new grid no more than this many times
	p.remap_target_order_span = 10.0;			// how many orders would you prefer your EEDF to span (in eV^-3/2)


	/* grid max start: use one or the other*/

	/*p.USE_EV_MAX_GUESS = false;
	p.USE_EV_MAX_GUESS = false;*/
	
	 p.initial_eV_max = 100; 

	 p.EN_Td = 100;
	 p.p_Torr = 760;
	 p.T_K = 300;


	 /* Perform calculation */

	 mb::BoltzmannSolver solver = mb::BoltzmannSolver(p, lib);	// perform the actual calculation
	 mb::BoltzmannOutput out = solver.get_output();			// get output struct

	
	std::string export_location = mb::UNALLOCATED_STRING;	// <- will do default location 
	std::string export_name = "example_with_excited_states";	// <- name of directory to export to
	mb::sweep_option sweep_op = mb::sweep_option::EN_Td; // <- tell the exporter to treat EN_Td as the independent variable


	mb::Exporter exp = mb::Exporter(lib, p, out, export_location, export_name, sweep_op);

	// then, do the actual writing of data
	exp.write_this_run(p, out);



	return 0;
}